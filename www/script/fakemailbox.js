"use strict";

console.warn("FakeMailbox is enabled. Remember to disable it before publishing the website!");


window.FakeMailbox = {
    DefaultMailbox: {
        isLastPage: true,
        nextPage: -1,
        messages: [
            {
                id: 123,
                replied: true,
                isUnread: false,
                sender: 'ZeroCrystal',
                subject: 'Hyper subject',
                opening: 'Ichi ni san shi go',
                time: '3 hours ago',
                body: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.'
            },

            {
                id: 42,
                replied: false,
                isUnread: false,
                sender: 'Tartaglia42',
                subject: 'BBcode test',
                opening: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel lacinia metus. Nunc rutrum id lectus quis blandit. Mauris mattis tincidunt interdum. Phasellus sit amet lectus placerat, efficitur ante vel, interdum quam. Sed condimentum turpis a maximus ullamcorper. Nunc id dolor sollicitudin, porta velit nec, dignissim justo. Integer sit amet aliquam risus. Pellentesque vel mi vestibulum lectus malesuada aliquet sit amet in arcu.',
                time: 'Dec 13, 5:47 PM',
                body: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel lacinia metus. Nunc rutrum id lectus quis blandit. Mauris mattis tincidunt interdum. Phasellus sit amet lectus placerat, efficitur ante vel, interdum quam. Sed condimentum turpis a maximus ullamcorper. Nunc id dolor sollicitudin, porta velit nec, dignissim justo. Integer sit amet aliquam risus. Pellentesque vel mi vestibulum lectus malesuada aliquet sit amet in arcu.<br>
<br>
Etiam at nibh massa. Cras facilisis tincidunt condimentum. Ut tempor felis ipsum, a sodales ligula scelerisque sit amet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce pretium rhoncus interdum. In lacinia nibh vel congue mattis. Morbi eget urna aliquam, luctus magna ac, efficitur nulla. Proin commodo rutrum nulla, vitae finibus urna ullamcorper non. Donec enim neque, congue vel rhoncus sodales, sollicitudin et velit. Ut hendrerit ac orci at convallis. Etiam vehicula velit et ante finibus porta. Vestibulum aliquam commodo scelerisque. Suspendisse tincidunt interdum iaculis.<br>
<br>
X <b>Bold</b> - this makes the text bold<br>
X <u>anda-rain</u> - this underlines the text<br>
X <i>itarikku</i> - this italicises the text<br>
X <span style="text-decoration:line-through;">sutoraiki</span> - this strikes through the text<br>
X <div style="text-align: center;">text</div><br>
X <div style="text-align: right;">text</div><br>
<br>
X <span style="color: blue">buru</span> - this changes the text color to blue<br>
<br>
You can also use colour codes to define what colour you want your text to be<br>
X <span style="color: #11FF11">Shiroi</span> - this changes the text color to white<br>
<br>
You can change the text size by using the tag, the @ZeroCrystal size is dependant on what value written. You can choose 20 to 200, which is representing the size in percent. <span style="font-size: 150%;">KOMAKAI</span>!!<br>
<br>
<iframe class="movie youtube" src="https://youtube.com/embed/eEUMAx3g6do?rel=1" width="425" height="355" frameborder="0"></iframe><br>
<br>
<ul><br>
<li>kawaii<br>
</li><li>fugu<br>
</li><li>shouen<br>
</li></ul> <br>
<br>
<ul><br>
<li>kawaii<br>
</li><li>fugu<br>
</li><li>shouen<br>
</li></ul><br>
<br>
<a href="https://myanimelist.net" rel="nofollow">Visit MyAnimeList</a><br>
<br>
<img class="userimg" src="https://cdn.myanimelist.net/images/userimages/10873739.jpg"><br>
<br>
<img class="userimg img-a-l" src="https://cdn.myanimelist.net/images/userimages/10873739.jpg"><br>
<br>
<img class="userimg img-a-r" src="https://cdn.myanimelist.net/images/userimages/10873739.jpg"><br>
<br>
<div class="spoiler"><input type="button" class="button show_button" onclick="this.nextSibling.style.display='inline-block';this.style.display='none';" data-showname="Show spoiler" data-hidename="Hide spoiler" value="Show spoiler"><span class="spoiler_content" style="display:none"><input type="button" class="button hide_button" onclick="this.parentNode.style.display='none';this.parentNode.parentNode.childNodes[0].style.display='inline-block';" value="Hide spoiler"><br>This is a spoiler for an episode of an anime that could make people angry</span></div><br>
<br>
<div class="spoiler"><input type="button" class="button show_button" onclick="this.nextSibling.style.display='inline-block';this.style.display='none';" data-showname="Show secret" data-hidename="Hide secret" value="Show secret"><span class="spoiler_content" style="display:none"><input type="button" class="button hide_button" onclick="this.parentNode.style.display='none';this.parentNode.parentNode.childNodes[0].style.display='inline-block';" value="Hide secret"><br>Secret</span></div><br>
<div class="spoiler"><input type="button" class="button show_button" onclick="this.nextSibling.style.display='inline-block';this.style.display='none';" data-showname="Show big secret" data-hidename="Hide big secret" value="Show big secret"><span class="spoiler_content" style="display:none"><input type="button" class="button hide_button" onclick="this.parentNode.style.display='none';this.parentNode.parentNode.childNodes[0].style.display='inline-block';" value="Hide big secret"><br>Big Secret</span></div><br>
<div class="spoiler"><input type="button" class="button show_button" onclick="this.nextSibling.style.display='inline-block';this.style.display='none';" data-showname="Show big secret" data-hidename="Hide big secret" value="Show big secret"><span class="spoiler_content" style="display:none"><input type="button" class="button hide_button" onclick="this.parentNode.style.display='none';this.parentNode.parentNode.childNodes[0].style.display='inline-block';" value="Hide big secret"><br>Big Secret</span></div><br>
<br>
<div class="codetext"><pre>You can make the text bold with [b]text[/b] tag.</pre></div><br>
<br>
<a href="https://myanimelist.net" rel="nofollow"><img class="userimg" src="https://cdn.myanimelist.net/images/userimages/10873739.jpg"></a><br>
<br>
<span style="color: red"><span style="font-size: 130%;"><b><u><i>itarikku</i></u></b></span></span>
<br>
<div class="quotetext">Nothing</div><br>
<br>
<div class="quotetext"><strong>XXXXXXXXXXXXXXXX said:</strong><br>Random person</div><br>
<br>
<div class="quotetext"><strong><a href="/forum/message/18239797?goto=topic">Kineta said:</a></strong><br>Real quote</div><br>
<br>
<div class="spoiler"><input type="button" class="button show_button" onclick="this.nextSibling.style.display='inline-block';this.style.display='none';" data-showname="Show quote" data-hidename="Hide quote" value="Show quote"><span class="spoiler_content" style="display:none"><input type="button" class="button hide_button" onclick="this.parentNode.style.display='none';this.parentNode.parentNode.childNodes[0].style.display='inline-block';" value="Hide quote"><br><div class="quotetext">Long quote: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel lacinia metus. Nunc rutrum id lectus quis blandit. Mauris mattis tincidunt interdum. Phasellus sit amet lectus placerat, efficitur ante vel, interdum quam. Sed condimentum turpis a maximus ullamcorper. Nunc id dolor sollicitudin, porta velit nec, dignissim justo. Integer sit amet aliquam risus. Pellentesque vel mi vestibulum lectus malesuada aliquet sit amet in arcu.<br>
<br>
Etiam at nibh massa. Cras facilisis tincidunt condimentum. Ut tempor felis ipsum, a sodales ligula scelerisque sit amet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce pretium rhoncus interdum. In lacinia nibh vel congue mattis. Morbi eget urna aliquam, luctus magna ac, efficitur nulla. Proin commodo rutrum nulla, vitae finibus urna ullamcorper non. Donec enim neque, congue vel rhoncus sodales, sollicitudin et velit. Ut hendrerit ac orci at convallis. Etiam vehicula velit et ante finibus porta. Vestibulum aliquam commodo scelerisque. Suspendisse tincidunt interdum iaculis.</div></span></div>
`
            },

            {
                id: 999,
                replied: true,
                isUnread: false,
                sender: 'ZeroCrystal',
                subject: 'Boring subject',
                opening: 'One two three four five fish',
                time: 'Nov 30, 6:57 PM',
                body: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.'
            }
        ]
    }
};


for (const message of FakeMailbox.DefaultMailbox.messages)
{
    Mailbox.storeMessage(message.id, adjustMessageBody(message.body));
}
