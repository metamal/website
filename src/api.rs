use lru_time_cache::LruCache;
use reqwest::{Method, Error};
use reqwest::blocking::Client;
use serde::Deserialize;
use rand::thread_rng;
use rusqlite::Connection;

use std::sync::{MutexGuard, Mutex, Arc};
use std::sync::mpsc::Sender;
use std::time::Duration;
use crate::UsageCache;
use crate::logging::*;
use crate::mailbox::{MailboxPage, Message};
use crate::storage::{get_session_id_for_user, get_usage_for_user, WriteBackOperation};
use crate::utility::{generate_random_string, build_new_thread_safe_http_client};


static API_URL: &'static str = "https://api.myanimelist.net/v3";


#[derive(Deserialize, Clone)]
pub struct AccessToken
{
    pub token_type: String,
    pub expires_in: i64,
    pub access_token: String,
    pub refresh_token: String,
}


#[derive(Deserialize, Clone)]
pub struct User
{
    pub id: i64,
    pub name: String,
    pub gender: Option<String>,
}


pub fn oauth_authorise_user(code: &str, code_verifier: &str, client_id: &str, client_secret: &str, client_guard: &MutexGuard<'_, Client>, logger: Logger) -> Result<AccessToken, Error>
{
    let url = "https://myanimelist.net/v1/oauth2/token";
    let parameters = [
        ("client_id", client_id),
        ("client_secret", client_secret),
        ("code", code),
        ("code_verifier", code_verifier),
        ("grant_type", "authorization_code")
    ];

    match client_guard.request(Method::POST, url)
        .form(&parameters)
        .timeout(Duration::from_secs(20))
        .send()?.error_for_status()
    {
        Ok(response) => Ok(response.json()?),
        Err(error) => {
            logger.send(LogMessage::Error(format!("MAL returned a bad status code when requesting a new Access Token: {}", error))).unwrap();
            Err(error)
        }
    }
}


impl AccessToken
{
    pub fn refresh(&self, client_id: &str, client_secret: &str, client_guard: MutexGuard<'_, Client>, logger: Logger) -> Result<AccessToken, Error>
    {
        let url = "https://myanimelist.net/v1/oauth2/token";
        let parameters = [
            ("client_id", client_id),
            ("client_secret", client_secret),
            ("refresh_token", &self.refresh_token),
            ("grant_type", "refresh_token")
        ];

        match client_guard.request(Method::POST, url)
            .form(&parameters)
            .timeout(Duration::from_secs(15))
            .send()?.error_for_status()
        {
            Ok(response) => Ok(response.json()?),
            Err(error) => {
                logger.send(LogMessage::Error(format!("MAL returned a bad status code when refreshing an existing Access Token: {}", error))).unwrap();
                Err(error)
            }
        }
    }


    pub fn get_user_info(&self, client_guard: &MutexGuard<'_, Client>, logger: Logger) -> Result<User, Error>
    {
        let url = format!("{}/users/@me?fields=id,name,gender", API_URL);

        match client_guard.request(Method::GET, &url)
            .bearer_auth(&self.access_token)
            .timeout(Duration::from_secs(15))
            .send()?.error_for_status()
        {
            Ok(response) => Ok(response.json()?),
            Err(error) => {
                logger.send(LogMessage::Error(format!("MAL returned a bad status code when retrieving user info: {}", error))).unwrap();
                Err(error)
            }
        }
    }
}

#[derive(Clone)]
pub struct UserApiUsage
{
    pub user_id: i64,
    pub monthly_requests: i64,
    pub monthly_limit: i64,
    pub write_back_sender: Sender<WriteBackOperation>,
}

impl UserApiUsage
{
    pub fn get_usage(user: &User, connection: &Connection, write_back_sender: Sender<WriteBackOperation>) -> UserApiUsage
    {
        get_usage_for_user(&user, &connection, write_back_sender)
    }


    pub fn get_default(user_id: i64, write_back_sender: Sender<WriteBackOperation>) -> UserApiUsage
    {
        UserApiUsage
        {
            user_id,
            monthly_requests: 0,
            monthly_limit: 5_000,
            write_back_sender,
        }
    }
}

pub fn can_send_requests(user_id: i64, usage_cache: &UsageCache, amount: i64, logger: Logger) -> Result<(), String>
{
    if let Some(usage) = usage_cache.lock().expect("Poisoned usage_cache lock in can_send_requests").get_mut(&user_id)
    {
        if usage.monthly_requests + amount > usage.monthly_limit {
            logger.send(LogMessage::Warning(format!("User {} exceeded their monthly quota", user_id))).unwrap();
            Err("You have reached your monthly quota of requests. You can <a href=\"/contact\" target=\"_blank\">contact me</a> to request a free increase.".to_string())
        } else {
            usage.monthly_requests += amount;

            match usage.write_back_sender.send(WriteBackOperation::UpdateUsage(usage.clone()))
            {
                Ok(_) => Ok(()),
                Err(error) => Err(format!("Cannot update your amount of monthly requests: {}", error.to_string()))
            }
        }
    } else {
        let message = format!("The Usage Cache doesn't have an entry for the current user (ID: {})\n", user_id);
        logger.send(LogMessage::error(&message)).unwrap();
        Err(message)
    }
}

#[derive(Clone)]
pub struct UserData
{
    pub user: User,
    pub token: AccessToken,
    pub session_id: String,
    pub http_client: Arc<Mutex<Client>>,
    pub mailbox_page_cache: Arc<Mutex<LruCache<usize, MailboxPage>>>,
    pub mailbox_message_cache: Arc<Mutex<LruCache<u64, Message>>>,
}

impl UserData
{
    pub fn from_access_token(token: AccessToken, client_guard: MutexGuard<'_, Client>, connection: MutexGuard<'_, Connection>, logger: Logger) -> Option<UserData>
    {
        let user = token.get_user_info(&client_guard, logger.clone()).ok()?;
        let session_id = get_session_id_for_user(&user, &connection, &logger)
            .unwrap_or_else(|| generate_random_string(512, &mut thread_rng()));
        let http_client = build_new_thread_safe_http_client();
        let mailbox_page_cache = Arc::new(Mutex::new(LruCache::with_expiry_duration_and_capacity(Duration::from_secs(60 * 10), 50)));
        let mailbox_message_cache = Arc::new(Mutex::new(LruCache::with_capacity(50)));

        Some(UserData { user, token, session_id, http_client, mailbox_page_cache, mailbox_message_cache })
    }
}

