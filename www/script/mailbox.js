"use strict";


/**
 * Remove all the "#text" nodes added by Bootstrap inside a MAL spoiler button.
 * @param {NodeList} childNodes - The list of child nodes.
 * @returns {Array} An array without any "#text" node.
 */
function fixChildNodes(childNodes)
{
    let filteredArray = [];
    for (const node of childNodes.values())
    {
        if (node.nodeName !== '#text')
            filteredArray.push(node);
    }

    return filteredArray;
}


/**
 * Skip all the "#text" nodes added by Bootstrap inside a MAL spoiler button.
 * @param {Node} element - The root node.
 * @returns {Node} The next non-"#text" sibling of the root node.
 */
function fixNextSibling(element)
{
    let sibling = element.nextSibling;
    while (sibling.nodeName === '#text')
        sibling = sibling.nextSibling;

    return sibling;
}


/**
 * Log an error message. The error is shown in both the console and in errorToast.
 * @param {string} message - The error message to log.
 */
function logError(message)
{
    console.error(message);

    const errorText = document.getElementById('errorToastBody');
    if (errorText.innerHTML.trim().length <= 0)
        errorText.innerHTML = message;
    else
        errorText.innerHTML = `${message}<hr>${errorText.innerHTML}`;

    $('#errorToast').toast('show');
}


/**
 * Get a random integer between the specified values.
 * @param {number} min - Minimum value of the range (inclusive).
 * @param {number} max - Maximum value of the range (exclusive).
 *
 * @returns {number} A random integer inside the range [min; max).
 */
function getRandomInt(min, max)
{
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
}


window.useDarkTheme = true;

/**
 * Switch between the dark and light themes
 */
function switchTheme()
{
    const htmlElement = document.documentElement;
    window.useDarkTheme = !useDarkTheme;

    if (window.useDarkTheme)
    {
        htmlElement.classList.add('dark');
        htmlElement.classList.remove('light');
    } else
    {
        htmlElement.classList.add('light');
        htmlElement.classList.remove('dark');
    }

    localStorage.setItem('useDarkTheme', window.useDarkTheme.toString());
}


/**
 * Log out the current user.
 */
function logOut()
{
    fetch('https://metamal.pro/oauth/logout', {'method': 'PATCH'})
        .then((response) =>
            {
                if (isOk(response))
                {
                    localStorage.clear();
                    sessionStorage.clear();
                    logError('Successfully logged out.');
                    window.location.replace('https://metamal.pro');
                }
            },
            (error) =>
            {
                localStorage.clear();
                sessionStorage.clear();
                logError(error);
            });
}


const DOWNLOADING_TAG = '(DOWNLOADING...)';


window.Mailbox = {
    lastMessageIndex: 0,
    currentQuery: '',
    nextQueryPage: 0,
    isLastPage: false,
    fetchBaseUrl: 'https://metamal.pro/mailbox/api/fetch',
    messageBodyBaseUrl: 'https://metamal.pro/mailbox/api/message',

    useSessionStorage: false,
    selectedIds: new Set(),

    /**
     * Return the encoded URL corresponding to the current query.
     * @returns {string} URL linked to a specific query.
     */
    getCurrentQueryUrl()
    {
        let url = new URL(this.fetchBaseUrl);

        if (this.currentQuery !== '')
            url.searchParams.set("query", this.currentQuery);

        if (this.nextQueryPage >= 0)
            url.searchParams.set("page", this.nextQueryPage);

        return url.toString();
    },


    /**
     * Return the encoded URL corresponding to the specified MAL message ID.
     * @param {number} messageId - A MAL message ID.
     * @returns {string} URL linked to the specified message.
     */
    getMessageBodyUrl(messageId)
    {
        let url = new URL(this.messageBodyBaseUrl);
        url.searchParams.set("id", messageId.toString());

        return url.toString();
    },

    /**
     * Return the right Storage interface (either localStorage (default) or sessionStorage).
     * @returns {Storage} A Storage interface.
     */
    getStorage()
    {
        return this.useSessionStorage ? sessionStorage : localStorage;
    },

    /**
     * Store a message inside the browser's cache.
     * @param {number | string} messageId - MAL ID of the message.
     * @param {string} htmlBody - Body of the message.
     */
    storeMessage(messageId, htmlBody)
    {
        const storage = this.getStorage();

        for (let i = 0; i < 5; i++)
        {
            try
            {
                storage.setItem(messageId.toString(), htmlBody);
                return;
            } catch (e)
            {
                this.freeSpaceInStorage(storage);
            }
        }

        try
        {
            storage.clear();
            console.warn('The message cache has been cleared.');
            storage.setItem(messageId.toString(), htmlBody);
        } catch (e)
        {
            logError(`Cannot add a new entry to the storage: ${e}`);
        }
    },

    /**
     * Remove some random entries from the storage.
     * @param {Storage} storage - A Storage interface.
     */
    freeSpaceInStorage(storage)
    {
        for (let i = 0; i < 20 && storage.length > 0; i++)
        {
            storage.removeItem(storage.key(getRandomInt(0, storage.length)));
        }
    },

    /**
     * Retrieve a message inside the browser's cache. Returns null if the key doesn't exist.
     * @param {number | string} messageId - MAL ID of the message.
     * @returns {string | null} Body of the message.
     */
    retrieveMessage(messageId)
    {
        return this.getStorage().getItem(messageId.toString());
    }
};


/**
 * Ensure that the response's "ok" property is set to true.
 * @param {Response} response - The Response object obtained using the Fetch API.
 * @returns {boolean} The "ok" value.
 */
function isOk(response)
{
    if (response.ok)
        return true;

    response.text().then(message =>
    {
        logError(`${message} (${response.status}: ${response.statusText})`);
    });

    return false;
}


/**
 * Retrieve and display the content of the default mailbox.
 */
function showDefaultMailbox()
{
    Mailbox.currentQuery = '';
    Mailbox.nextQueryPage = 0;
    Mailbox.isLastPage = false;

    fetchAndShowMailboxFromUrl(Mailbox.getCurrentQueryUrl(), true);
}


/**
 * Retrieve and display the content of the mailbox at the specified URL.
 * @param {string} url - A URL corresponding to a mailbox query.
 * @param {boolean} removeExistingMessages - Specify whether the messages already displayed in the accordion should be removed or not.
 */
function fetchAndShowMailboxFromUrl(url, removeExistingMessages)
{
    disableLoadOlderMessagesButton();
    setLoadingTextForQueryLabel();
    getMailboxAccordion(removeExistingMessages);

    fetch(url)
        .then((response) =>
            {
                if (isOk(response))
                    response.json().then(value => showMailbox(value, removeExistingMessages));
                enableSearchBar();
            },
            (error) =>
            {
                logError(error);
                removeMailboxSpinner();
                disableLoadOlderMessagesButton();
                enableSearchBar();
            });
}


/**
 * Display the content of the provided mailbox.
 * @param {Object} mailbox - A mailbox object containing the messages to be displayed.
 * @param {boolean} removeExistingMessages - Specify whether the messages already displayed in the accordion should be removed or not.
 */
function showMailbox(mailbox, removeExistingMessages)
{
    const mailboxAccordion = getMailboxAccordion(removeExistingMessages);

    if (removeExistingMessages)
        Mailbox.lastMessageIndex = 0;

    for (const message of mailbox.messages)
        appendMessage(message, mailboxAccordion, Mailbox.lastMessageIndex++);

    Mailbox.isLastPage = mailbox.isLastPage;
    Mailbox.nextQueryPage = mailbox.nextPage;

    removeMailboxSpinner();
    if (!mailbox.isLastPage)
        enableLoadOlderMessagesButton();

    updateQueryLabel();
}


/**
 * Get a reference to the "mailboxAccordion" element after, if specified, removing all its inner elements.
 * @param {boolean} removeExistingMessages - Specify whether the messages already displayed in the accordion should be removed or not.
 * @returns {HTMLElement} The "mailboxAccordion" HTML element.
 */
function getMailboxAccordion(removeExistingMessages)
{
    const mailboxAccordion = document.getElementById("mailboxAccordion");
    const spinnerHTML = getMailboxSpinnerHTML();

    if (removeExistingMessages)
    {
        mailboxAccordion.innerHTML = spinnerHTML;
        Mailbox.selectedIds.clear();
    } else if (document.getElementById('mailboxSpinner') === null)
        mailboxAccordion.innerHTML += spinnerHTML;

    return mailboxAccordion;
}


/**
 * Get the HTML for the mailbox spinner.
 * @returns {string} An HTML string.
 */
function getMailboxSpinnerHTML()
{
    return '<div id="mailboxSpinner" class="d-flex justify-content-center"><div class="spinner-grow mailbox-spinner" role="status"><span class="sr-only">Loading…</span></div></div>';
}


/**
 * Remove the "mailboxSpinner" element.
 */
function removeMailboxSpinner()
{
    document.getElementById("mailboxSpinner")?.remove();
}


/**
 * Append the given message to the mailbox accordion.
 * @param {Object} message - The message to append.
 * @param {HTMLElement} accordion - A reference to the "mailboxAccordion" HTML element.
 * @param {number} index - The index of the message.
 */
function appendMessage(message, accordion, index)
{
    let buffer = `<div class="${message.isUnread && Mailbox.retrieveMessage(message.id) === null ? 'card-unread' : 'card'}"><div class="card-header container-fluid"><div class="row"><div class="col-2 align-self-center">`;
    buffer += `<span class="material-icons replied-icon ${message.replied ? '' : 'invisible'}" data-toggle="tooltip" title="Replied">reply</span>\n `;

    if (message.sender)
        buffer += `<a class="sender-link" href="https://myanimelist.net/profile/${message.sender}" rel="nofollow external noreferrer noopener" target="_blank">${message.sender}</a>`;

    buffer += `</div><div class="col-8 align-self-center"><button class="btn btn-link card-button btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse${index}"> <span class="subject-line">${message.subject}</span> <span class="opening-line"> ‣ ${message.opening}</span>`;

    buffer += `</button></div><div class="col-2 align-self-center right-text-align">${message.time}</div>`;

    buffer += `</div></div><div id="collapse${index}"  class="collapse" data-parent="#mailboxAccordion"><div id="card-body-${message.id}" class="card-body"><div class="d-flex justify-content-center"><div class="spinner-grow message-spinner" role="status"><span class="sr-only">Loading…</span></div></div></div></div></div>\n`;

    accordion.innerHTML += buffer;

    // setTimeout is required to force the browser to update the DOM.
    setTimeout(function (container, messageId)
    {
        $(container).on('show.bs.collapse', () =>
        {
            loadMessageBody(messageId);
        });
    }, 10, `#collapse${index}`, message.id);

}


/**
 * Enable the "Load older messages" button.
 */
function enableLoadOlderMessagesButton()
{
    document.getElementById('loadOlderMessagesButton')
        .disabled = false;
}


/**
 * Disable the "Load older messages" button.
 */
function disableLoadOlderMessagesButton()
{
    document.getElementById('loadOlderMessagesButton')
        .disabled = true;
}


/**
 * Adjust the HTML of the message's body to better adapt it to the mailbox.
 * @param {string} htmlText - The body of the message (formatted as a HTML string).
 * @returns {string} The altered version of the text.
 */
function adjustMessageBody(htmlText)
{
    // Enable the "privacy enhanced mode" to all YouTube iframes.
    htmlText = htmlText.replaceAll('src="https://youtube.com/embed/', 'src="https://www.youtube-nocookie.com/embed/');

    // Fix JS issues related to the spoiler buttons and Bootstrap.
    htmlText = htmlText.replaceAll('this.parentNode.parentNode.childNodes', 'fixChildNodes(this.parentNode.parentNode.childNodes)');
    htmlText = htmlText.replaceAll('this.nextSibling', 'fixNextSibling(this)');

    // Open all links in a new tab.
    htmlText = htmlText.replaceAll('rel="nofollow">', 'rel="nofollow external noreferrer noopener" target="_blank">');
    htmlText = htmlText.replaceAll('<a rel="nofollow"', '<a rel="nofollow external noreferrer noopener" target="_blank"');
    htmlText = htmlText.replaceAll('<div class="quotetext"><strong><a', '<div class="quotetext"><strong><a rel="nofollow external noreferrer noopener" target="_blank"');

    // Convert all relative URLs into absolute URLs inside [quote] elements.
    htmlText = htmlText.replaceAll('href="/forum/message/', 'href="https://myanimelist.net/forum/message/');

    // Replace some CSS colour keywords with similar, more dark-theme-friendly colours.
    if (window.useDarkTheme)
    {
        const colours = [['red', 'E57373'], ['purple', 'BA68C8'], ['fuchsia', 'EA80FC'], ['green', '81C784'], ['lime', 'DCE775'], ['yellow', 'FFF176'],
            ['blue', '64B5F6'], ['teal', '4DB6AC'], ['aqua', '4FC3F7'], ['orange', 'FFB74D'], ['aquamarine', '4DD0E1'], ['brown', 'A1887F'], ['coral', 'FF8A65'],
            ['cyan', '4FC3F7'], ['gold', 'FFD54F'], ['indigo', '7986CB'], ['magenta', 'EA80FC'], ['pink', 'F06292'], ['violet', '9575CD'], ['gray', 'E0E0E0'], ['grey', 'E0E0E0']];

        for (const [keyword, rgb] of colours)
            htmlText = htmlText.replaceAll(`<span style="color: ${keyword}">`, `<span style="color: #${rgb}">`);
    }

    return htmlText;
}


/**
 * Expand the current list of messages by adding older entities corresponding to the latest submitted query.
 */
function loadOlderMessages()
{
    if (Mailbox.isLastPage)
    {
        disableLoadOlderMessagesButton();
        return;
    }

    fetchAndShowMailboxFromUrl(Mailbox.getCurrentQueryUrl(), false);
}


/**
 * Retrieve and display the content of the specified message. This operation is cached.
 * @param {number} messageId - The MAL ID of the message.
 */
function loadMessageBody(messageId)
{
    const message = Mailbox.retrieveMessage(messageId);
    if (message === DOWNLOADING_TAG)
        return;

    if (message === null)
    {
        Mailbox.storeMessage(messageId, DOWNLOADING_TAG);

        fetch(Mailbox.getMessageBodyUrl(messageId))
            .then((response) =>
                {
                    if (isOk(response))
                        response.json().then(response =>
                        {
                            let htmlBody = response.body;
                            Mailbox.storeMessage(messageId, htmlBody);
                            htmlBody = adjustMessageBody(htmlBody);
                            displayMessageBody(messageId, htmlBody);
                        });
                },
                (error) =>
                {
                    logError(error);
                });
    } else
    {
        displayMessageBody(messageId, adjustMessageBody(message));
    }
}


/**
 * Display the content of a message inside the right container.
 * @param {number} messageId - The MAL ID of the message.
 * @param {string} htmlBody - The formatted body of the message.
 */
function displayMessageBody(messageId, htmlBody)
{
    const messageContainer = document.getElementById(`card-body-${messageId}`);
    if (messageContainer)
        messageContainer.innerHTML = `<div><a href="https://myanimelist.net/mymessages.php?go=read&id=${messageId}" role="button" class="btn message-button no-underline" rel="nofollow external noreferrer noopener" target="_blank">View on <img class="MAL-mini-image dark" src="/assets/MAL-white.png" alt="MAL logo white"><img class="MAL-mini-image light" src="/assets/MAL-blue.png" alt="MAL logo blue"><sup><span class="material-icons open-in-new-tab-icon">open_in_new</span></sup></a></div>${htmlBody}`;

    const messageCard = messageContainer?.parentElement?.parentElement;
    if (messageCard !== null && messageCard.classList.contains('card-unread'))
    {
        setTimeout(function (container)
        {
            container.classList.add('card');
            container.classList.remove('card-unread');
        }, 2000, messageCard);
    }
}


/**
 * Catch the "Enter" key from the search bar and run the query.
 * @param {KeyboardEvent} event - The MAL ID of the message.
 */
function runNewQuery(event)
{
    if (event.key !== "Enter" && event.code !== "Enter" && event.code !== "NumpadEnter")
        return;

    const query = document.getElementById("mailbox-search").value;

    if (query.trim() === '')
    {
        showDefaultMailbox();
        return;
    }

    if (query.trim().length < 3)
    {
        logError("Invalid search query!");
        return;
    }

    Mailbox.currentQuery = query;
    Mailbox.nextQueryPage = 0;
    Mailbox.isLastPage = false;

    disableSearchBar();
    fetchAndShowMailboxFromUrl(Mailbox.getCurrentQueryUrl(), true);
}


/**
 * Disable the search bar.
 */
function disableSearchBar()
{
    document.getElementById("mailbox-search")
        .readOnly = true;
}


/**
 * Re-enable the search bar.
 */
function enableSearchBar()
{
    document.getElementById("mailbox-search")
        .readOnly = false;
}


/**
 * Update the text of the query label located on top of the "Load older messages" button.
 */
function updateQueryLabel()
{
    const found = document.getElementById('mailboxAccordion').childElementCount;
    const queried = Mailbox.isLastPage ? 'ALL' : Mailbox.nextQueryPage * 20;

    document.getElementById('query-label').innerText = `Found: ${found} | Queried: ${queried}`;
}


/**
 * Set the text of the query label to "Loading…".
 */
function setLoadingTextForQueryLabel()
{
    document.getElementById('query-label').innerText = 'Loading…';
}


/**
 * Catch the "change" event of a message's checkbox.
 * @param {Event} event - A "change" event.
 */
function updateCheckboxSelection(event)
{
    const messageId = event.target.id.slice(9);  // E.g.: checkbox-123

    if (event.target.checked)
        Mailbox.selectedIds.add(messageId);
    else
        Mailbox.selectedIds.delete(messageId);
}


// Test LocalStorage and SessionStorage
Mailbox.storeMessage('TEST', 'ZeroCrystal');
if (Mailbox.retrieveMessage('TEST') !== 'ZeroCrystal')
{
    Mailbox.useSessionStorage = true;

    Mailbox.storeMessage('TEST', 'ZeroCrystal');
    if (Mailbox.retrieveMessage('TEST') !== 'ZeroCrystal')
        logError('🔥 FATAL: MetaMAL cannot access neither sessionStorage nor localStorage. At least one of the two storage mechanisms is required to use Mailbox.' +
            'Try updating your browser to the latest version or whitelisting "https://metamal.pro".\n\nAs long as you keep receiving this message you won\'t be able' +
            'to use MetaMAL Mailbox.');
}

Mailbox.getStorage().removeItem('TEST');


// Remove all stored messages with the DOWNLOADING_TAG (i.e. they failed to download during a previous session)
for (let i = 0; i < Mailbox.getStorage().length; i++)
    if (Mailbox.getStorage().getItem(Mailbox.getStorage().key(i)) === DOWNLOADING_TAG)
        Mailbox.getStorage().removeItem(Mailbox.getStorage().key(i));
