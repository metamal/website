const baseColors = [
    ["#FF7961", "#F44336"],  // Red 500
    ["#6EC6FF", "#2196F3"],  // Blue 500
    ["#FFF350", "#FFC107"],  // Amber 500
    ["#D05CE3", "#9C27B0"],  // Purple 500
    ["#BEF67A", "#8BC34A"],  // Light Green 500
    ["#FF6090", "#E91E63"],  // Pink 500
    ["#8EACBB", "#607D8B"],  // Blue Gray 500
    ["#FF8A50", "#FF5722"],  // Deep Orange 500
    ["#A98274", "#795548"],  // Brown 500
    ["#52C7B8", "#009688"],  // Teal 500

    ["#FFFF72", "#FFEB3B"],  // Yellow 500
    ["#80E27E", "#4CAF50"],  // Green 500
    ["#9A67EA", "#673AB7"],  // Deep Purple 500
    ["#FFC947", "#FF9800"],  // Orange 500
    ["#67DAFF", "#03A9F4"],  // Light Blue 500
    ["#FFFF6E", "#CDDC39"],  // Lime 500
    ["#757DE8", "#3F51B5"],  // Indigo 500
    ["#CFCFCF", "#9E9E9E"],  // Grey 500
    ["#62EFFF", "#00BCD4"],  // Cyan 500

    ["#FF5131", "#D50000"],  // Red A700
    ["#768FFF", "#2962FF"],  // Blue A700
    ["#FFDD4B", "#FFAB00"],  // Amber A700
    ["#E254FF", "#AA00FF"],  // Purple A700
    ["#9CFF57", "#64DD17"],  // Light Green A700
    ["#FD558F", "#C51162"],  // Pink A700
    ["#62727B", "#37474F"],  // Blue Gray 800
    ["#FF6434", "#DD2C00"],  // Deep Orange A700
    ["#7B5E57", "#4E342E"],  // Brown 800
    ["#5DF2D6", "#00BFA5"],  // Teal A700

    ["#FFFF52", "#FFD600"],  // Yellow A700
    ["#5EFC82", "#00C853"],  // Green A700
    ["#9D46FF", "#6200EA"],  // Deep Purple A700
    ["#FF9E40", "#FF6D00"],  // Orange A700
    ["#64C1FF", "#0091EA"],  // Light Blue A700
    ["#E4FF54", "#AEEA00"],  // Lime A700
    ["#7A7CFF", "#304FFE"],  // Indigo A700
    ["#6D6D6D", "#424242"],  // Grey 800
    ["#62EBFF", "#00B8D4"]  // Cyan A700
];

let index = 0;
let couple = null;
let changeIndex = false;

Chart.defaults.global.defaultFontFamily = '"Noto Sans", "Noto Sans JP", "Noto Sans HK", "Noto Sans KR", "Noto Sans TC", "Noto Sans SC", sans-serif';
Chart.defaults.global.defaultFontSize = 12;
Chart.defaults.global.legend.position = "bottom";


function getBorderColor()
{
    checkIndex();
    return couple[0];
}

function getBackgroundColor()
{
    const element = couple[1];
    checkIndex();
    return element;
}

function checkIndex()
{
    if (changeIndex)
        index++;
    if (index >= baseColors.length)
        index = 0;
    changeIndex = !changeIndex;
    couple = baseColors[index];
}
