let showChartDialog = true;
let showSpreadsheetDialog = true;


function exportChart(button, canvasId) {
    if (showChartDialog) {
        window.alert("Don't forget to give credit to MetaMAL if you're going to transform and/or share this image.  ;-)");
        showChartDialog = false;
    }

    const exportingCanvas = document.getElementById(canvasId);
    button.href = exportingCanvas.toDataURL("image/png");
}


function exportSpreadsheet() {
    if (showChartDialog) {
        window.alert("The spreadsheet is divided into several tabs, one for each anime category. You can most likely find them at the bottom left corner of your preferred editor.");
        showChartDialog = false;
    }
}


let showDatasets = true;

function toggleChartVisibility(chart) {
    for (const dataset of chart.data.datasets) {
        dataset.hidden = showDatasets;

        if (dataset._meta != undefined) {
            for (const field of Object.values(dataset._meta)) {
                field.hidden = showDatasets;
            }
        }
    }

    chart.update();
    showDatasets = !showDatasets;
}
