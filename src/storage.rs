use crate::UsageCache;
use crate::api::{User, AccessToken, UserApiUsage, UserData};
use crate::logging::*;
use crate::utility::{build_new_thread_safe_http_client, build_new_http_client};

use chrono::{Local, Duration, DateTime, FixedOffset, Datelike};
use lru_time_cache::LruCache;
use reqwest::Method;
use reqwest::blocking::Client;
use rusqlite::{Connection, params, Result as DbResult, Error, NO_PARAMS};

use std::iter::FromIterator;
use std::sync::{MutexGuard, Arc, Mutex};
use std::sync::mpsc::{channel, Sender, Receiver, TryRecvError};
use std::thread;
use std::time::Duration as StdDuration;
use std::thread::sleep;


static DATABASE_PATH: &str = "./users.sqlite";


pub fn generate_database_if_needed(client_id: String, client_secret: String, users_cache: Arc<Mutex<LruCache<String, UserData>>>, logger: Logger) -> (Connection, Sender<WriteBackOperation>)
{
    let connection = Connection::open(DATABASE_PATH)
        .expect("Cannot open or create the SQLite database");

    connection.execute(
        "CREATE TABLE IF NOT EXISTS Users
            (
                UserID                             INT
                                                       CONSTRAINT Users_pk
                                                       PRIMARY KEY,
                Username                           TEXT NOT NULL,
                Gender                             TEXT,
                SessionID                          TEXT,
                Requests                           INT DEFAULT 0 NOT NULL,
                \"Requests limit\"                 INT DEFAULT 5000 NOT NULL,
                \"Refresh token\"                  TEXT,
                \"Refresh token expiration date\"  TEXT,
                \"Refresh errors\"                 INT DEFAULT 0
            );

            CREATE UNIQUE INDEX IF NOT EXISTS UserID_index ON Users (UserID);

            CREATE UNIQUE INDEX IF NOT EXISTS SessionID_index ON Users (SessionID);",
        params![]).expect("Cannot create the 'Users' database table");

    let (sender, receiver) = channel();
    let write_back_connection = Connection::open(DATABASE_PATH)
        .expect("Cannot open the SQLite database");
    let refresher_connection = Connection::open(DATABASE_PATH)
        .expect("Cannot open the SQLite database");

    start_database_write_back_thread(write_back_connection, receiver, logger.clone());
    start_refresh_token_updater_thread(client_id, client_secret, users_cache, refresher_connection, logger);
    (connection, sender)
}


fn start_database_write_back_thread(connection: Connection, channel: Receiver<WriteBackOperation>, logger: Logger)
{
    thread::spawn(move || {
        loop
        {
            // Run all queued operations
            let mut possible_operation = channel.try_recv();
            while possible_operation.is_ok()
            {
                execute_operation(possible_operation.expect("'possible_operation' MUST NOT be an error at this point"), &connection, &logger);
                possible_operation = channel.try_recv();
            }

            // Terminate the thread if the channel was closed or sleep for a few seconds otherwise
            let error = possible_operation.err().expect("'possible_operation' MUST be an error at this point");
            match error {
                TryRecvError::Empty => thread::sleep(StdDuration::from_secs(3)),
                TryRecvError::Disconnected => break
            }
        }

        println!("<Database write-back thread terminated>");
    });
}


fn start_refresh_token_updater_thread(client_id: String, client_secret: String, users_cache: Arc<Mutex<LruCache<String, UserData>>>, connection: Connection, logger: Logger)
{
    thread::spawn(move || {
        loop
        {
            reset_api_usage_at_the_start_of_each_month(&connection, &logger);

            match get_all_refresh_tokens(&connection, &logger)
            {
                Ok(tokens) => refresh_all_tokens(tokens, &users_cache, &client_id, &client_secret, &connection, &logger),
                Err(error) => logger.send(LogMessage::Error(format!("Failed to get all Refresh Tokens from the database: {}", error))).unwrap()
            };

            logger.send(LogMessage::info("Daily refresh routine completed")).unwrap();
            sleep(StdDuration::from_secs(60 * 60 * 24));  // = sleep(24 hours)
        }
    });
}


struct RefreshToken
{
    user_id: i64,
    session_id: Option<String>,
    token: Option<String>,
    expiration_date: Option<DateTime<FixedOffset>>,
    errors: i64,
}


fn reset_api_usage_at_the_start_of_each_month(connection: &Connection, logger: &Logger)
{
    if Local::today().day() != 1 {
        return;
    }

    let result = connection.execute(
        "UPDATE Users SET Requests = 0;", NO_PARAMS,
    );

    if let Err(error) = result {
        logger.send(LogMessage::Error(format!("Failed to reset the API usage of all users: {}", error))).unwrap();
    } else {
        logger.send(LogMessage::info("Reset API usage of all users")).unwrap();
    }
}


fn get_all_refresh_tokens(connection: &Connection, logger: &Logger) -> Result<Vec<RefreshToken>, String>
{
    let mut query = connection.prepare("SELECT UserID, SessionID, \"Refresh token\", \"Refresh token expiration date\", \"Refresh errors\" FROM Users").expect("Invalid SQL query in get_all_refresh_tokens");

    let rows = query.query_map(NO_PARAMS, |row| {
        let timestamp_row: DbResult<String> = row.get(3);
        let expiration_date = if let Ok(timestamp) = timestamp_row {
            DateTime::parse_from_rfc3339(timestamp.as_str()).ok()
        } else {
            None
        };

        Ok(
            RefreshToken {
                user_id: row.get(0).expect("A database row doesn't contain the UserID"),
                session_id: row.get(1).ok(),
                token: row.get(2).ok(),
                expiration_date,
                errors: row.get(4).unwrap_or_default(),
            }
        )
    });

    match rows
    {
        Ok(rows_ok) => Ok(Vec::from_iter(
            rows_ok.map(|token| token.expect("The query mustn't return any Err in get_all_refresh_tokens"))
        )),
        Err(error) => {
            logger.send(LogMessage::Error(format!("Failed to retrieve all Refresh Tokens from the database: {}", error))).unwrap();
            Err(error.to_string())
        }
    }
}


fn refresh_all_tokens(tokens: Vec<RefreshToken>, users_cache: &Arc<Mutex<LruCache<String, UserData>>>, client_id: &str, client_secret: &str, connection: &Connection, logger: &Logger)
{
    let client = build_new_http_client();

    tokens
        .iter()
        .filter(|token| token.token.is_some()
            && !token.token.clone().unwrap().is_empty()
            && token.expiration_date.is_some()
            && token.expiration_date.clone().unwrap() - Duration::days(7) <= Local::now())
        .for_each(|token| {
            sleep(StdDuration::from_secs(1));
            refresh_token(token, client_id, client_secret, users_cache, &client, connection, logger);
        });
}


fn refresh_token(token: &RefreshToken, client_id: &str, client_secret: &str, users_cache: &Arc<Mutex<LruCache<String, UserData>>>, http_client: &Client, connection: &Connection, logger: &Logger) -> RefreshToken
{
    let url = "https://myanimelist.net/v1/oauth2/token";
    let refresh_token = token.token.clone().expect("All tokens passed to refresh_token must have a valid Refresh Token");
    let parameters = [
        ("client_id", client_id),
        ("client_secret", client_secret),
        ("refresh_token", refresh_token.as_str()),
        ("grant_type", "refresh_token")
    ];

    let fresh_token = match http_client.request(Method::POST, url)
        .form(&parameters)
        .timeout(StdDuration::from_secs(20))
        .send().expect("Failed to send a POST request from refresh_token").error_for_status()
    {
        Ok(response) => {
            let api_token: AccessToken = response.json().expect("Failed to convert a JSON Access Token received from MAL");
            RefreshToken {
                user_id: token.user_id,
                session_id: token.session_id.clone(),
                token: Some(api_token.refresh_token),
                expiration_date: Some(DateTime::from(Local::now() + Duration::days(30))),
                errors: 0,
            }
        }
        Err(error) => {
            logger.send(LogMessage::Error(format!("MAL returned a bad status code when refreshing an existing Token in a background thread: {}", error))).unwrap();
            RefreshToken {
                user_id: token.user_id,
                session_id: token.session_id.clone(),
                token: token.token.clone(),
                expiration_date: token.expiration_date,
                errors: token.errors + 1,
            }
        }
    };

    set_refresh_token_for_user(&fresh_token, connection, logger);
    invalidate_old_token(&fresh_token, users_cache);
    fresh_token
}


fn invalidate_old_token(token: &RefreshToken, users_cache: &Arc<Mutex<LruCache<String, UserData>>>)
{
    if token.errors > 0 || token.session_id.is_none() {
        return;
    }

    let session_id = token.session_id.clone().unwrap();
    users_cache
        .lock()
        .expect("Poisoned users_cache lock in invalidate_old_token")
        .remove(&session_id);
}


pub enum WriteBackOperation
{
    UpdateUserData(UserData),
    UpdateUsage(UserApiUsage),
    LogoutUser(String),
}


fn execute_operation(operation: WriteBackOperation, connection: &Connection, logger: &Logger)
{
    match operation
    {
        WriteBackOperation::UpdateUserData(user) => update_user_data(user, connection, logger),
        WriteBackOperation::UpdateUsage(user) => set_usage_for_user(user, connection, logger),
        WriteBackOperation::LogoutUser(session_id) => remove_session_id(session_id, connection, logger)
    }
}


fn update_user_data(data: UserData, connection: &Connection, logger: &Logger)
{
    let token_expiration = (Local::now() + Duration::days(30)).to_rfc3339();

    let result = connection.execute(
        "REPLACE INTO Users
             (UserID, Username, Gender, SessionID, \"Refresh token\", \"Refresh token expiration date\")
             VALUES (?1, ?2, ?3, ?4, ?5, ?6)
             ON CONFLICT (UserID) DO UPDATE SET
             Username = ?2,
             Gender = ?3,
             SessionID = ?4,
             \"Refresh token\" = ?5,
             \"Refresh token expiration date\" = ?6
             WHERE UserID = ?1;",
        params![data.user.id, data.user.name, data.user.gender.unwrap_or_default(), data.session_id, data.token.refresh_token, token_expiration]);

    if let Err(error) = result
    {
        logger.send(LogMessage::Error(format!("Failed to execute query to update {} ({}) user's data: {}", data.user.name, data.user.id, error))).unwrap();
    }
}


pub fn get_session_id_for_user(user: &User, connection: &Connection, logger: &Logger) -> Option<String>
{
    let result: DbResult<String> = connection.query_row("SELECT SessionID FROM Users WHERE UserID = ?1;",
                                                        params![user.id], |row| row.get(0));

    if let Err(error) = result
    {
        match error
        {
            Error::QueryReturnedNoRows => {
                logger.send(LogMessage::Info(format!("The database doesn't contain any row corresponding to the user {} ({}). Generating a new Session ID.", user.name, user.id))).unwrap();
            }
            error => {
                logger.send(LogMessage::Warning(format!("Failed to retrieve the Session ID for user {} ({}) from the database. A new one will be generated. Error: {}", user.name, user.id, error))).unwrap();
            }
        };

        return None;
    }

    let session_id = result.unwrap();
    if session_id.is_empty() { None } else { Some(session_id) }
}


pub fn set_session_id_for_user(user: &User, session_id: &str, connection: &Connection, logger: &Logger)
{
    let result = connection.execute(
        "UPDATE Users SET
             SessionID = ?1
             WHERE UserID = ?2;",
        params![session_id, user.id]);

    if let Err(error) = result
    {
        logger.send(LogMessage::Error(format!("Failed to set the Session ID for user {} ({}): {}", user.name, user.id, error))).unwrap();
    }
}


pub fn get_usage_for_user(user: &User, connection: &Connection, write_back_sender: Sender<WriteBackOperation>) -> UserApiUsage
{
    let result: DbResult<UserApiUsage> = connection.query_row("SELECT Requests, \"Requests limit\" FROM Users WHERE UserID = ?1;",
                                                              params![user.id],
                                                              |row| Ok(UserApiUsage {
                                                                  user_id: user.id,
                                                                  monthly_requests: row.get(0).unwrap(),
                                                                  monthly_limit: row.get(1).unwrap(),
                                                                  write_back_sender: write_back_sender.clone(),
                                                              }));

    result.unwrap_or_else(|_| UserApiUsage::get_default(user.id, write_back_sender))
}


pub fn set_usage_for_user(data: UserApiUsage, connection: &Connection, logger: &Logger)
{
    let result = connection.execute(
        "UPDATE Users SET
             Requests = ?1
             WHERE UserID = ?2;",
        params![data.monthly_requests, data.user_id]);

    if let Err(error) = result
    {
        logger.send(LogMessage::Error(format!("Failed to set the API usage for user ID {}: {}", data.user_id, error))).unwrap();
    }
}


fn set_refresh_token_for_user(data: &RefreshToken, connection: &Connection, logger: &Logger)
{
    let result = if data.errors == 0 && data.token.is_some() && data.expiration_date.is_some()
    {
        connection.execute(
            "UPDATE Users SET
                'Refresh token' = ?1,
                'Refresh token expiration date' = ?2,
                'Refresh errors' = ?3
                 WHERE UserID = ?4;",
            params![data.token.clone().unwrap(), data.expiration_date.clone().unwrap().to_rfc3339(), data.errors, data.user_id])
    } else {
        connection.execute(
            "UPDATE Users SET
                'Refresh errors' = ?1
                 WHERE UserID = ?2;",
            params![data.errors, data.user_id])
    };

    if let Err(error) = result
    {
        logger.send(LogMessage::Error(format!("Failed to set the Refresh Token for user ID {} (errors counter: {}): {}", data.user_id, data.errors, error))).unwrap();
    }
}


pub fn get_user_by_session_id(session_id: &str, connection: MutexGuard<Connection>, write_back_sender: Sender<WriteBackOperation>, usage_cache: UsageCache, logger: &Logger) -> Option<UserData>
{
    if session_id.is_empty() {
        return None;
    }

    let result: DbResult<UserData> = connection.query_row("SELECT UserID, Username, Gender, Requests, \"Requests limit\", \"Refresh token\"
                                                           FROM Users WHERE SessionID = ?1;",
                                                          params![session_id], |row| {
            let user = Ok(UserData {
                user: User {
                    id: row.get(0).unwrap(),
                    name: row.get(1).unwrap(),
                    gender: row.get(2).unwrap_or_default(),
                },
                token: AccessToken {
                    token_type: "".to_string(),
                    expires_in: 0,
                    access_token: "".to_string(),
                    refresh_token: row.get(5).unwrap(),
                },
                session_id: session_id.to_string(),
                http_client: build_new_thread_safe_http_client(),
                mailbox_page_cache: Arc::new(Mutex::new(LruCache::with_expiry_duration_and_capacity(StdDuration::from_secs(60 * 10), 50))),
                mailbox_message_cache: Arc::new(Mutex::new(LruCache::with_capacity(50))),
            });

            let usage = UserApiUsage {
                user_id: row.get(0).unwrap(),
                monthly_requests: row.get(3).unwrap(),
                monthly_limit: row.get(4).unwrap(),
                write_back_sender,
            };

            usage_cache
                .lock()
                .expect("Poisoned usage_cache lock in get_user_by_session_id")
                .insert(usage.user_id, usage);

            user
        });

    if let Err(error) = result
    {
        match error
        {
            Error::QueryReturnedNoRows => {
                logger.send(LogMessage::Warning(format!("The database doesn't contain any user corresponding to the provided Session ID ({})", session_id))).unwrap();
            }
            error => {
                logger.send(LogMessage::Error(format!("Failed to get a user from their Session ID: {} (Session ID: {})", error, session_id))).unwrap();
            }
        };

        return None;
    }

    let data = result.unwrap();
    if data.session_id.is_empty() {
        None
    } else {
        Some(data)
    }
}


fn remove_session_id(session_id: String, connection: &Connection, logger: &Logger)
{
    let result = connection.execute(
        "UPDATE Users SET
             SessionID = NULL
             WHERE SessionID = ?1;",
        params![session_id]);

    if let Err(error) = result
    {
        logger.send(LogMessage::Error(format!("Failed to remove the Session ID of a user: {} (Session ID: {})", error, session_id))).unwrap();
    }
}
