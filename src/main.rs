#![feature(proc_macro_hygiene, decl_macro)]

mod api;
mod logging;
mod mailbox;
mod seasons_data;
mod storage;
mod utility;

extern crate chrono;
extern crate lru_time_cache;
extern crate num_format;
extern crate rand;
extern crate reqwest;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
extern crate rusqlite;
extern crate serde;
extern crate serde_json;
extern crate time;

use api::{oauth_authorise_user, UserData};
use lru_time_cache::LruCache;
use rand::{thread_rng};
use reqwest::blocking::Client;
use rocket_contrib::json::{Json, JsonValue};
use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::{Template};
use rocket::{Request, Response, State};
use rocket::fairing::{AdHoc};
use rocket::http::{Status, Cookies, Cookie, SameSite, RawStr};
use rocket::http::ext::IntoOwned;
use rocket::http::uri::Absolute;
use rocket::request::{FromRequest, Outcome};
use rocket::response::content::{Html};
use rocket::response::{Redirect, status};
use rusqlite::Connection;
use seasons_data::{*};
use storage::{generate_database_if_needed, get_user_by_session_id, WriteBackOperation};
use time::{Duration, now};
use utility::{get_path_for_season_banner, generate_random_string};

use std::collections::{BTreeSet, BTreeMap};
use std::convert::Infallible;
use std::fmt::{Display, Formatter, Result as FmtResult};
use std::fs::File;
use std::iter::FromIterator;
use std::mem::drop;
use std::ops::Deref;
use std::process::exit;
use std::sync::{Arc, Mutex, RwLock};
use std::sync::atomic::{AtomicBool};
use std::sync::mpsc::{Sender};
use std::thread::sleep;
use std::time::Duration as StdDuration;
use crate::api::UserApiUsage;
use crate::logging::*;
use crate::mailbox::{run_mailbox_query, QueryResult, Message, get_message};
use crate::utility::build_new_thread_safe_http_client;


fn main()
{
    rocket::ignite()
        .mount("/", StaticFiles::from(utility::get_website_path()).rank(50))
        .mount("/character", StaticFiles::from(utility::get_characters_pictures_path()))
        .mount("/seiyuu", StaticFiles::from(utility::get_seiyuu_pictures_path()))
        .mount("/spreadsheet", StaticFiles::from(utility::get_spreadsheet_pictures_path()))
        .mount("/", routes![index, contact, about, terms, security, archive, seasonal_latest, seasonal, seasonal_full, oauth_login, oauth_callback, oauth_logout, mailbox, mailbox_live, fetch_mailbox_page, fetch_message])
        .register(catchers![not_found, bad_request, internal_server_error])
        .attach(AdHoc::on_response("Headers changer", change_headers))
        .attach(Template::custom(|engines| {
            engines.tera.autoescape_on(vec![]);
            engines.tera.register_function("season_title", Box::new(get_season_title));
            engines.tera.register_function("labels", Box::new(get_timestamp_labels));
            engines.tera.register_function("anime_data", Box::new(get_formatted_anime_data));
            engines.tera.register_function("hottest", Box::new(get_formatted_hottest_characters_or_seiyuu));
            engines.tera.register_function("genres", Box::new(get_formatted_trending_genres));
        }))
        .manage(build_startup_context())
        .launch();
}


#[get("/")]
fn index() -> Html<File>
{
    Html(
        File::open(utility::get_website_path().join("index.html"))
            .expect("Cannot open 'index.html'")
    )
}


#[get("/about")]
fn about() -> Redirect
{
    Redirect::moved(uri!(contact))
}


#[get("/contact")]
fn contact() -> Html<File>
{
    Html(
        File::open(utility::get_website_path().join("contact.html"))
            .expect("Cannot open 'contact.html'")
    )
}


#[get("/terms")]
fn terms(context: State<AppContext>) -> Html<File>
{
    context.get_logger().send(LogMessage::info("Someone accessed '/terms'")).unwrap();

    Html(
        File::open(utility::get_website_path().join("terms.html"))
            .expect("Cannot open 'terms.html'")
    )
}


#[get("/.well-known/security.txt")]
fn security(context: State<AppContext>) -> Html<File>
{
    context.get_logger().send(LogMessage::info("Someone accessed '/.well-known/security.txt'")).unwrap();

    Html(
        File::open(utility::get_website_path().join(".well-known").join("security.txt"))
            .expect("Cannot open 'security.txt'")
    )
}


#[get("/seasonal/archive")]
fn archive(context: State<AppContext>) -> Result<Template, Status>
{
    if let Ok(data) = context.seasons_data.read() {
        return Ok(Template::render("archive",
                                   get_archive_context(data.deref())));
    }
    Err(Status::NotFound)
}


#[get("/seasonal")]
fn seasonal_latest(context: State<AppContext>) -> Redirect
{
    let data = context
        .seasons_data.read()
        .expect("Cannot read AppContext");

    Redirect::to(uri!(seasonal: data.last_year, data.last_season.to_lowercase()))
}


#[get("/seasonal/<year>/<season>")]
fn seasonal(year: i32, season: String, context: State<AppContext>) -> Result<Template, Status>
{
    if let Some(start_end) = context.seasons_data.read()
        .expect("Cannot read AppContext")
        .get_data(year, &season) {
        if let Some(statistics) = SeasonStatistics::load(&season, year, false) {
            return Ok(Template::render("seasonal",
                                       SeasonalContext {
                                           banner: get_path_for_season_banner(year, &season),
                                           start_end,
                                           statistics,
                                           season,
                                           year,
                                       }));
        }
    }
    Err(Status::NotFound)
}


#[get("/seasonal/full/<year>/<season>")]
fn seasonal_full(year: i32, season: String, context: State<AppContext>) -> Result<Template, Status>
{
    if let Some(start_end) = context.seasons_data.read()
        .expect("Cannot read AppContext")
        .get_data(year, &season) {
        if let Some(statistics) = SeasonStatistics::load(&season, year, true) {
            return Ok(Template::render("seasonal_full",
                                       SeasonalContext {
                                           banner: get_path_for_season_banner(year, &season),
                                           start_end,
                                           statistics,
                                           season,
                                           year,
                                       }));
        }
    }
    Err(Status::NotFound)
}


#[get("/oauth/login?<from>")]
fn oauth_login(from: Option<String>, mut cookies: Cookies, context: State<AppContext>) -> Result<Redirect, Status>
{
    let logger = context.get_logger();

    // Check if the user is already logged in
    if get_user_from_cookies(&mut cookies, &context).is_ok()
    {
        let url = match from.unwrap_or_default().as_str()
        {
            "live-mailbox" => "/mailbox/live",
            _ => "/"
        };

        return Ok(Redirect::to(url));
    }

    logger.send(LogMessage::info("Someone started the OAuth login procedure")).unwrap();

    // Generate a random Code Verifier / Code Challenge
    let mut rng = thread_rng();
    let code_verifier = generate_random_string(128, &mut rng);

    // Store the Code Verifier in a private cookie
    &cookies.add_private(
        Cookie::build("CodeVerifier", code_verifier.clone())
            .http_only(true)
            .expires(now() + Duration::minutes(5))
            .secure(true)
            .same_site(SameSite::Lax)
            .finish());

    // Redirect the user to the authorisation page
    let url = match from {
        Some(state) => format!("https://myanimelist.net/v1/oauth2/authorize?response_type=code&client_id={}&code_challenge={}&state={}", context.client_id, code_verifier, state),
        None => format!("https://myanimelist.net/v1/oauth2/authorize?response_type=code&client_id={}&code_challenge={}", context.client_id, code_verifier)
    };

    if let Ok(uri) = Absolute::parse(&url)
    {
        Ok(Redirect::to(uri.into_owned()))
    } else {
        logger.send(LogMessage::Error(format!("Failed to build the authorisation URL: {}", url))).unwrap();
        Err(Status::BadRequest)
    }
}


#[get("/oauth?<code>&<state>")]
fn oauth_callback(code: &RawStr, state: Option<String>, mut cookies: Cookies<'_>, user_agent: UserAgent, context: State<'_, AppContext>) -> Result<Template, Status>
{
    let logger = context.get_logger();

    // Log in the requesting user
    logger.send(LogMessage::Info(format!("Someone is trying to exchange an Authorisation Code for a Session ID (UA: {})", user_agent))).unwrap();
    let authorisation_code = code.percent_decode_lossy();
    if authorisation_code.is_empty() {
        logger.send(LogMessage::Error(format!("The user-provided Authorisation Code is empty (UA: {})", user_agent))).unwrap();
        return Err(Status::BadRequest);
    }

    if let Some(code_verifier) = cookies.get_private("CodeVerifier")
    {
        let client_guard = context.http_client.lock().expect("Poisoned http_client lock in oauth_callback");
        if let Ok(token) = oauth_authorise_user(&authorisation_code, code_verifier.value(), &context.client_id, &context.client_secret, &client_guard, logger.clone())
        {
            let db_connection_guard = context.db_connection.lock().expect("Poisoned db_connection lock in oauth_callback");
            if let Some(user) = UserData::from_access_token(token, client_guard, db_connection_guard, logger.clone())
            {
                // Remove the Code Verifier and store a Session ID
                &cookies.remove_private(code_verifier);

                &cookies.add_private(
                    Cookie::build("SessionID", user.session_id.clone())
                        .http_only(true)
                        .permanent()
                        .secure(true)
                        .same_site(SameSite::Strict)
                        .finish());

                // Save the Session ID in permanent storage
                {
                    context.db_write_back_sender.lock().expect("Poisoned db_write_back_sender lock in oauth_callback").send(
                        WriteBackOperation::UpdateUserData(user.clone()))
                        .expect("Cannot send an UpdateUserData operation to the DB thread");
                }

                // Cache the API usage
                {
                    context.usage_cache.lock().expect("Poisoned usage_cache lock in oauth_callback")
                        .insert(user.user.id, UserApiUsage::get_usage(&user.user,
                                                                      &context.db_connection.lock().expect("Poisoned db_connection lock in oauth_callback"),
                                                                      context.db_write_back_sender.lock().expect("Poisoned db_write_back_sender lock in oauth_callback").clone()));
                }

                // Cache the UserData and Session ID
                {
                    context.users_cache.lock().expect("Poisoned users_cache lock in oauth_callback")
                        .insert(user.session_id.clone(), user.clone());
                }

                // Redirect the user according to the "state" parameter contained inside the OAuth login request
                let url = match state.unwrap_or_default().as_str()
                {
                    "live-mailbox" => "/mailbox/live",
                    _ => "/"
                };

                logger.send(LogMessage::Info(format!("{} ({}) logged in successfully (state: {} | UA: {})", user.user.name, user.user.id, url, user_agent))).unwrap();
                let mut context = BTreeMap::new();
                context.insert("url".to_string(), url.to_string());

                return Ok(Template::render("oauth-redirect", context));
            } else {
                &cookies.remove_private(code_verifier);
                logger.send(LogMessage::Error(format!("Failed to obtain the user info using their Access Token (UA: {})", user_agent))).unwrap();
            }
        } else {
            &cookies.remove_private(code_verifier);
            logger.send(LogMessage::Error(format!("Failed to authenticate the user using the provided Authorisation Code (UA: {})", user_agent))).unwrap();
        }
    } else {
        let cookies_list = String::from_iter(
            cookies
                .iter()
                .map(|cookie| format!("\n    - Name: {} | Value: {}", cookie.name(), cookie.value()))
        );

        logger.send(LogMessage::Error(format!("Cannot access the 'CodeVerifier' cookie (UA: {}). Existing cookies:{}", user_agent, cookies_list))).unwrap();
    }

    Err(Status::BadRequest)
}


#[patch("/oauth/logout")]
fn oauth_logout(mut cookies: Cookies<'_>, context: State<'_, AppContext>) -> Result<JsonValue, status::BadRequest<String>>
{
    let logger = context.get_logger();

    if context.logout_user(&mut cookies) {
        logger.send(LogMessage::info("A user logged out from the website")).unwrap();

        Ok(
            json!({"result": "OK"})
        )
    } else {
        logger.send(LogMessage::error("Someone failed to log out from the website (invalid/missing Session ID?)")).unwrap();

        Err(
            status::BadRequest(
                Some(
                    "What are you trying to do exactly? You're either already logged out or you've stored an invalid Session ID.".to_string()
                )
            )
        )
    }
}


#[get("/mailbox")]
fn mailbox() -> Html<File>
{
    Html(
        File::open(utility::get_website_path().join("mailbox.html"))
            .expect("Cannot open 'mailbox.html'")
    )
}


#[get("/mailbox/live")]
fn mailbox_live() -> Html<File>
{
    Html(
        File::open(utility::get_website_path().join("mailbox-live.html"))
            .expect("Cannot open 'mailbox-live.html'")
    )
}


fn get_user_from_cookies(cookies: &mut Cookies<'_>, context: &State<'_, AppContext>) -> Result<UserData, status::BadRequest<String>>
{
    match context.get_user(cookies)
    {
        Some(user) => Ok(user),
        None => Err(
            status::BadRequest(
                Some("You are not logged in or your Session ID is invalid".to_string())
            )
        )
    }
}


#[get("/mailbox/api/fetch?<query>&<page>")]
fn fetch_mailbox_page(query: Option<String>, page: Option<usize>, mut cookies: Cookies<'_>, context: State<'_, AppContext>) -> Result<Json<QueryResult>, status::BadRequest<String>>
{
    let logger = context.get_logger();

    match get_user_from_cookies(&mut cookies, &context)
    {
        Ok(mut user) => {
            match run_mailbox_query(query.clone(), page, &mut user, context.get_usage_cache(), logger.clone())
            {
                Ok(result) => Ok(Json(result)),
                Err(error) => {
                    logger.send(LogMessage::Error(format!("Failed to fetch the requested Mailbox page. User: {} ({}) | Query: {} | Page: {}", user.user.name, user.user.id, query.unwrap_or("None".to_string()), page.unwrap_or_default()))).unwrap();
                    Err(status::BadRequest(Some(error)))
                }
            }
        }
        Err(error) => {
            logger.send(LogMessage::error("Someone tried to fetch a Mailbox page without being properly authenticated")).unwrap();
            Err(error)
        }
    }
}


#[get("/mailbox/api/message?<id>")]
fn fetch_message(id: u64, mut cookies: Cookies<'_>, context: State<'_, AppContext>) -> Result<Json<Message>, status::BadRequest<String>>
{
    let logger = context.get_logger();

    match get_user_from_cookies(&mut cookies, &context)
    {
        Ok(mut user) => {
            match get_message(id, &mut user, context.get_usage_cache(), logger.clone())
            {
                Ok(result) => Ok(Json(result)),
                Err(error) => {
                    logger.send(LogMessage::Error(format!("Failed to fetch the requested Mailbox message. User: {} ({}) | ID: {}", user.user.name, user.user.id, id))).unwrap();
                    Err(status::BadRequest(Some(error)))
                }
            }
        }
        Err(error) => {
            logger.send(LogMessage::error("Someone tried to fetch a Mailbox message without being properly authenticated")).unwrap();
            Err(error)
        }
    }
}


#[catch(400)]
fn bad_request(_req: &Request) -> Html<File> {
    Html(
        File::open(utility::get_website_path().join("400.html"))
            .expect("Cannot open '400.html'")
    )
}


#[catch(404)]
fn not_found(_req: &Request) -> Html<File> {
    Html(
        File::open(utility::get_website_path().join("404.html"))
            .expect("Cannot open '404.html'")
    )
}


#[catch(500)]
fn internal_server_error(_req: &Request) -> Html<File> {
    Html(
        File::open(utility::get_website_path().join("500.html"))
            .expect("Cannot open '500.html'")
    )
}


fn build_startup_context() -> AppContext
{
    utility::panic_on_missing_crawler_directories();

    let context = AppContext::new();

    if context.client_id.is_empty() || context.client_secret.is_empty()
    {
        eprintln!("You forgot to set either client_id or client_secret!");
        exit(1);
    }

    seasons_data::start_seasons_data_updater_thread(
        context.seasons_data.clone(),
        context.stop_seasons_data_updater.clone(),
    );

    context
}


type UsageCache = Box<Arc<Mutex<LruCache<i64, UserApiUsage>>>>;


struct AppContext
{
    client_id: String,
    client_secret: String,

    log_sender: Mutex<Logger>,

    seasons_data: Arc<RwLock<SeasonsData>>,
    stop_seasons_data_updater: Arc<AtomicBool>,

    http_client: Arc<Mutex<Client>>,

    //                              \/ Session ID
    users_cache: Arc<Mutex<LruCache<String, UserData>>>,
    //                                 \/ Session ID
    login_requests: Arc<Mutex<BTreeSet<String>>>,
    //                              \/ User ID
    usage_cache: Arc<Mutex<LruCache<i64, UserApiUsage>>>,

    db_connection: Arc<Mutex<Connection>>,
    db_write_back_sender: Arc<Mutex<Sender<WriteBackOperation>>>,
}


impl AppContext
{
    fn new() -> AppContext
    {
        let client_id = "".to_string();
        let client_secret = "".to_string();
        let logger = start_logging_thread();
        let users_cache = Arc::new(Mutex::new(LruCache::with_expiry_duration_and_capacity(StdDuration::from_secs(60 * 50), 500)));

        let (db_connection_temp, sender_temp) = generate_database_if_needed(client_id.clone(), client_secret.clone(), users_cache.clone(), logger.clone());

        AppContext
        {
            client_id,
            client_secret,

            log_sender: Mutex::new(logger),

            seasons_data: Arc::new(
                RwLock::new(
                    SeasonsData::load()
                )
            ),
            stop_seasons_data_updater: Arc::new(
                AtomicBool::new(false)
            ),

            http_client: build_new_thread_safe_http_client(),

            users_cache,
            login_requests: Arc::new(Mutex::new(BTreeSet::new())),
            usage_cache: Arc::new(Mutex::new(LruCache::with_expiry_duration_and_capacity(StdDuration::from_secs(60 * 60), 550))),

            db_connection: Arc::new(Mutex::new(db_connection_temp)),
            db_write_back_sender: Arc::new(Mutex::new(sender_temp)),
        }
    }


    fn get_user(&self, cookies: &mut Cookies) -> Option<UserData>
    {
        if let Some(session_cookie) = cookies.get_private("SessionID")
        {
            let mut users_cache_lock = self.users_cache.lock().expect("Poisoned users_cache lock in AppContext.get_user");
            if let Some(user) = users_cache_lock.get(session_cookie.value())
            {
                let user_clone = user.clone();
                drop(users_cache_lock);
                return Some(user_clone);
            } else {
                drop(users_cache_lock);

                // Avoid having two or more threads querying the Users database in parallel,
                // and thus generating multiple Access Tokens.
                let mut login_requests_lock = self.login_requests.lock().expect("Poisoned login_requests lock in AppContext.get_user");
                if login_requests_lock.get(session_cookie.value()).is_some()
                {
                    // Another thead is currently waiting for a reply from the Users database.
                    // We just wait for the value to appear inside users_cache.
                    drop(login_requests_lock);

                    loop
                    {
                        sleep(StdDuration::from_millis(50));
                        users_cache_lock = self.users_cache.lock().expect("Poisoned users_cache lock in AppContext.get_user");
                        if let Some(user) = users_cache_lock.get(session_cookie.value())
                        {
                            let user_clone = user.clone();
                            drop(users_cache_lock);
                            return Some(user_clone);
                        }
                    }
                }

                // This is the first thread requesting the user's data.
                // We signal it to the rest of the application and contact the Users database.
                login_requests_lock.insert(session_cookie.value().to_string());
                drop(login_requests_lock);

                let write_back_guard = self.db_write_back_sender.lock().expect("Poisoned db_write_back_sender lock in AppContext.get_user");
                let logger = self.get_logger();
                if let Some(mut db_user) = get_user_by_session_id(session_cookie.value(), self.db_connection.lock().expect("Poisoned db_connection lock in AppContext.get_user"), write_back_guard.clone(), self.get_usage_cache(), &logger)
                {
                    if let Ok(new_token) = db_user.token.refresh(&self.client_id, &self.client_secret, self.http_client.lock().expect("Poisoned http_client lock in AppContext.get_user"), logger)
                    {
                        db_user.token = new_token;
                    } else {
                        cookies.remove_private(session_cookie);
                        return None;
                    }

                    write_back_guard.send(WriteBackOperation::UpdateUserData(db_user.clone())).unwrap();
                    self.users_cache.lock().expect("Poisoned users_cache lock in AppContext.get_user")
                        .insert(session_cookie.value().to_string(), db_user.clone());
                    self.login_requests.lock().expect("Poisoned login_requests lock in AppContext.get_user")
                        .remove(session_cookie.value());

                    return Some(db_user);
                } else {
                    drop(write_back_guard);
                    self.login_requests.lock().expect("Poisoned login_requests lock in AppContext.get_user")
                        .remove(session_cookie.value());
                    cookies.remove_private(session_cookie);
                }
            }
        }

        None
    }


    fn logout_user(&self, cookies: &mut Cookies) -> bool
    {
        if let Some(session_cookie) = cookies.get_private("SessionID")
        {
            let session_id = session_cookie.value().to_string();

            {
                let mut users_cache_lock = self.users_cache.lock().expect("Poisoned users_cache lock in AppContext.logout_user");
                users_cache_lock.remove(&session_id);
            }

            {
                let db_lock = self.db_write_back_sender.lock().expect("Poisoned db_write_back_sender lock in AppContext.logout_user");
                db_lock.send(WriteBackOperation::LogoutUser(session_id)).unwrap();
            }

            cookies.remove_private(session_cookie);
            return true;
        }

        false
    }


    fn get_usage_cache(&self) -> UsageCache
    {
        Box::from(self.usage_cache.clone())
    }


    fn get_logger(&self) -> Logger
    {
        self.log_sender
            .lock()
            .expect("Poisoned log_sender lock in get_logger")
            .clone()
    }
}


#[derive(Clone, Debug)]
struct UserAgent(String);


impl FromRequest<'_, '_> for UserAgent
{
    type Error = Infallible;

    fn from_request(request: &Request) -> Outcome<Self, Self::Error>
    {
        Outcome::Success(
            UserAgent(
                request
                    .headers()
                    .get_one("User-Agent")
                    .unwrap_or("Unknown")
                    .to_string()
            )
        )
    }
}


impl Display for UserAgent
{
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult
    {
        write!(f, "{}", self.0)
    }
}


fn change_headers(_: &Request, response: &mut Response)
{
    // Disable Federated Learning of Cohorts (FLoC)
    response.set_raw_header("Permissions-Policy", "interest-cohort=()");

    // ❤
    response.set_raw_header("Server", "Aperture Science's Hypertextual Transfer Device");
    response.set_raw_header("X-Powered-By", "Aperture Science's Hypertextual Transfer Device");
    response.set_raw_header("Hey", "Why are you inspecting this HTTP packet? You meanie!");
}
