use rand::Rng;
use rand::distributions::Alphanumeric;
use reqwest::blocking::Client;

use std::{fs, iter};
use std::path::{Path};
use std::sync::{Mutex, Arc};
use std::time::Duration as StdDuration;


const CRAWLER_PATH: &str = "../crawler";
const WEBSITE_PATH: &str = "www";

pub fn get_crawler_path() -> Box<Path>
{
    fs::canonicalize(CRAWLER_PATH)
        .expect("Cannot access 'crawler' directory")
        .into_boxed_path()
}

pub fn get_data_path() -> Box<Path>
{
    get_crawler_path()
        .join(Path::new("data"))
        .canonicalize()
        .expect("Cannot access 'data' directory")
        .into_boxed_path()
}

pub fn get_database_path() -> Box<Path>
{
    get_crawler_path()
        .join(Path::new("database"))
        .canonicalize()
        .expect("Cannot access 'database' directory")
        .into_boxed_path()
}

pub fn get_pictures_path() -> Box<Path>
{
    get_crawler_path()
        .join(Path::new("pictures"))
        .canonicalize()
        .expect("Cannot access 'pictures' directory")
        .into_boxed_path()
}

pub fn get_characters_pictures_path() -> Box<Path>
{
    get_pictures_path()
        .join(Path::new("characters"))
        .canonicalize()
        .expect("Cannot access 'characters' directory")
        .into_boxed_path()
}

pub fn get_seiyuu_pictures_path() -> Box<Path>
{
    get_pictures_path()
        .join(Path::new("seiyuu"))
        .canonicalize()
        .expect("Cannot access 'seiyuu' directory")
        .into_boxed_path()
}

pub fn get_spreadsheet_pictures_path() -> Box<Path>
{
    get_data_path()
        .join(Path::new("spreadsheet"))
        .canonicalize()
        .expect("Cannot access 'spreadsheet' directory")
        .into_boxed_path()
}

pub fn get_website_path() -> Box<Path>
{
    fs::canonicalize(WEBSITE_PATH)
        .expect("Cannot access 'www' directory")
        .into_boxed_path()
}

pub fn get_assets_path() -> Box<Path>
{
    get_website_path()
        .join(Path::new("assets"))
        .canonicalize()
        .expect("Cannot access 'assets' directory")
        .into_boxed_path()
}

pub fn get_banners_path() -> Box<Path>
{
    get_assets_path()
        .join(Path::new("banners"))
        .canonicalize()
        .expect("Cannot access 'banners' directory")
        .into_boxed_path()
}

pub fn panic_on_missing_crawler_directories()
{
    let printer = |p: Box<Path>| println!("OK: {}", p.display());

    printer(get_crawler_path());
    printer(get_database_path());
    printer(get_data_path());
    printer(get_characters_pictures_path());
    printer(get_seiyuu_pictures_path());
    printer(get_banners_path());
}

pub fn get_filename_for_character_or_person_picture(id: i32, is_character: bool) -> String
{
    let path = if is_character {
        get_characters_pictures_path()
    } else {
        get_seiyuu_pictures_path()
    };

    let extension = if path.join(format!("{}.jpg", id)).exists() {
        "jpg".to_string()
    } else if path.join(format!("{}.png", id)).exists() {
        "png".to_string()
    } else if path.join(format!("{}.gif", id)).exists() {
        "gif".to_string()
    } else if path.join(format!("{}.webp", id)).exists() {
        "webp".to_string()
    } else {
        return "/assets/not-found.png".to_string();
    };

    if is_character {
        format!("/character/{}.{}", id, extension).to_string()
    } else {
        format!("/seiyuu/{}.{}", id, extension).to_string()
    }
}

pub fn get_path_for_season_banner(year: i32, season: &str) -> String
{
    if get_banners_path().join(format!("{}-{}.jpg", year, season)).exists() {
        format!("/assets/banners/{}-{}.jpg", year, season)
    } else {
        "/assets/banners/not-found.jpg".to_string()
    }
}

pub fn generate_random_string<T: Rng>(length: usize, rng: &mut T) -> String
{
    iter::repeat(())
        .map(|()| rng.sample(Alphanumeric))
        .take(length)
        .collect()
}

pub fn build_new_thread_safe_http_client() -> Arc<Mutex<Client>>
{
    Arc::new(
        Mutex::new(
            build_new_http_client()
        )
    )
}

pub fn build_new_http_client() -> Client
{
    Client::builder()
        .connect_timeout(StdDuration::from_secs(20))
        .gzip(true)
        .timeout(StdDuration::from_secs(15))
        .user_agent(format!("MetaMAL.pro API backend / {}", env!("CARGO_PKG_VERSION")))
        .https_only(true)
        .build().expect("Cannot build Reqwest's HTTP client")
}
