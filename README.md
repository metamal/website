# Overview
MetaMAL is composed of two main and interconnected projects:
- **[Crawler](https://gitlab.com/metamal/crawler)**: it **collects** and **analyzes statistical data** taken from [MyAnimeList](https://myanimelist.net);
- **[Website](https://gitlab.com/metamal/website)**: it **converts** the data gathered by the crawler into a **user-friendly format** and makes it **accessible** through the web.

This repository aims to provide a **fast** and **stable HTTP web server** able to re-elaborate the raw data collected by the [Crawler](https://gitlab.com/metamal/crawler) into a **pleasant HTML page**.

At the moment the only service offered by MetaMAL is a **restyled** and **enriched version** of the traditional MyAnimeList's **[seasonal anime page](https://myanimelist.net/anime/season)**. More tools will be developed in the near future.

You can find the live version of the website on [https://MetaMAL.pro](https://metamal.pro).

## Technical details
The website's backend is fully written in **[Rust](https://www.rust-lang.org)** and based on the **[Rocket](https://rocket.rs)** web framework. **[Tera](https://tera.netlify.com)** is employed for rendering all HTML templates.

Check out the official [Rocket documentation](https://rocket.rs/v0.4/guide/getting-started/) for detailed instructions on how to **build** and **run the project** from source code.

The [Crawler](https://gitlab.com/metamal/crawler) is **required** in order to execute the web server. The relative path of the root folder is specified by the **[`CRAWLER_PATH`](https://gitlab.com/metamal/website/blob/master/src/utility.rs#L4)** variable.

Once launched, the web server will listen for incoming HTTP requests on port **`8090`**.
