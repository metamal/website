use std::fs::{OpenOptions, File, create_dir_all};
use std::io::{BufWriter, Write};
use std::sync::mpsc::{channel, Sender, Receiver};
use std::thread;
use chrono::Local;

#[derive(Clone)]
pub enum LogMessage
{
    Info(String),
    Warning(String),
    Error(String),
}

pub type Logger = Sender<LogMessage>;


impl LogMessage
{
    pub fn info(message: &str) -> LogMessage
    {
        LogMessage::Info(message.to_string())
    }

    pub fn warning(message: &str) -> LogMessage
    {
        LogMessage::Warning(message.to_string())
    }

    pub fn error(message: &str) -> LogMessage
    {
        LogMessage::Error(message.to_string())
    }
}


pub fn start_logging_thread() -> Sender<LogMessage>
{
    create_dir_all("logs").expect("Cannot create the './logs' folder!");

    let info_file = BufWriter::new(
        OpenOptions::new()
            .create(true)
            .append(true)
            .open("logs/info.log")
            .expect("Cannot open the './logs/info.log' file!")
    );

    let warning_file = BufWriter::new(
        OpenOptions::new()
            .create(true)
            .append(true)
            .open("logs/warning.log")
            .expect("Cannot open the './logs/warning.log' file!")
    );

    let error_file = BufWriter::new(
        OpenOptions::new()
            .create(true)
            .append(true)
            .open("logs/error.log")
            .expect("Cannot open the './logs/error.log' file!")
    );

    let (sender, receiver) = channel();

    thread::spawn(move || {
        log_messages(receiver, info_file, warning_file, error_file);
    });

    sender
}


fn log_messages(receiver: Receiver<LogMessage>, mut info_file: BufWriter<File>, mut warning_file: BufWriter<File>, mut error_file: BufWriter<File>)
{
    for log_message in receiver.iter()
    {
        match log_message
        {
            LogMessage::Info(message) => write_to_file(message, &mut info_file, false),
            LogMessage::Warning(message) => write_to_file(message, &mut warning_file, true),
            LogMessage::Error(message) => write_to_file(message, &mut error_file, true)
        };
    }
}


fn write_to_file(message: String, file: &mut BufWriter<File>, log_to_console: bool)
{
    let formatted_message = add_header(message);

    if log_to_console {
        eprintln!("{}", &formatted_message);
    }

    file.write(
        formatted_message
            .as_bytes()
    ).expect("Cannot log message to file");

    file.flush().expect("Cannot flush log file");
}


fn add_header(message: String) -> String
{
    let timestamp = Local::now().format("%Y %b %d - %H:%M:%S %Z").to_string();
    format!("[{}] {}\n", timestamp, message)
}
