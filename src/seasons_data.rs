use num_format::{Locale, ToFormattedString};
use rocket_contrib::templates::tera::{Value, Error};
use serde::{Serialize, Deserialize};
use time::{now_utc};
use std::collections::{BTreeMap, HashMap};
use std::fs::File;
use std::io::BufReader;
use std::sync::{Arc, RwLock};
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread;
use std::time::Duration;
use crate::utility;


#[derive(Serialize, Deserialize)]
pub struct SeasonsData
{
    #[serde(alias = "first season")]
    pub first_season: String,
    #[serde(alias = "first year")]
    pub first_year: i32,
    #[serde(alias = "last season")]
    pub last_season: String,
    #[serde(alias = "last year")]
    pub last_year: i32,
    #[serde(alias = "seasons")]
    pub seasons_by_year: BTreeMap<String, BTreeMap<String, String>>
}


#[derive(Serialize, Deserialize)]
pub struct SeasonStartEnd
{
    start: String,
    end: String
}


impl SeasonsData
{
    pub fn load() -> SeasonsData
    {
        let file_path = utility::get_data_path()
            .join("seasons.json");

        let file = File::open(file_path)
            .expect("Cannot open 'data/seasons.json'");

        let reader = BufReader::new(file);
        let mut result: SeasonsData = serde_json::from_reader(reader)
            .expect("Failed to convert 'data/seasons.json' into SeasonsData");
        result.first_season = result.first_season.to_lowercase();
        result.last_season = result.last_season.to_lowercase();
        result
    }

    pub fn update(&mut self)
    {
        let new_data = SeasonsData::load();
        self.first_season = new_data.first_season;
        self.first_year = new_data.first_year;
        self.last_season = new_data.last_season;
        self.last_year = new_data.last_year;
        self.seasons_by_year = new_data.seasons_by_year;
    }

    pub fn get_data(&self, year: i32, season: &str) -> Option<SeasonStartEnd>
    {
        if let Some(year_map) = self.seasons_by_year.get(&year.to_string()) {
            if let Some(season_start) = year_map.get(format!("{} start", season.to_uppercase()).as_str()) {
                if let Some(season_end) = year_map.get(format!("{} end", season.to_uppercase()).as_str()) {
                    return Some(SeasonStartEnd { start: season_start.into(), end: season_end.into() });
                }
            }
        }
        None
    }
}


pub fn start_seasons_data_updater_thread(seasons_data: Arc<RwLock<SeasonsData>>, should_stop: Arc<AtomicBool>)
{
    thread::spawn(move || {
        let mut last_update = now_utc();

        while !should_stop.load(Ordering::Acquire) {
            let now = now_utc();
            // Update seasonal data every day at 00:30 UTC.
            // The crawler usually completes its job around at 00:15 UTC.
            if now.tm_hour == 0 && now.tm_min == 30 && now.tm_mday != last_update.tm_mday
            {
                seasons_data
                    .write()
                    .expect("Cannot update SeasonsData from dedicated thread")
                    .update();
                last_update = now;
            }

            thread::sleep(Duration::from_secs(5));
        }
    });
}


#[derive(Serialize, Deserialize)]
pub struct AnimeEntry
{
    #[serde(alias = "Id")]
    pub id: i32,
    #[serde(alias = "Title")]
    pub title: String,
    #[serde(alias = "Score")]
    pub score: Option<f64>,
    #[serde(alias = "Members")]
    pub members: Option<i32>,
    #[serde(alias = "Timestamp")]
    pub timestamp: String
}


#[derive(Serialize, Deserialize)]
pub struct CharacterOrSeiyuuEntry
{
    #[serde(alias = "Id")]
    pub id: i32,
    #[serde(alias = "Name")]
    pub name: String,
    #[serde(alias = "Members")]
    pub members: i32,
    #[serde(alias = "Timestamp")]
    pub timestamp: String
}


#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub enum CharacterOrSeiyuuFields
{
    Delta(i32),
    Entries(Vec<CharacterOrSeiyuuEntry>)
}


#[derive(Serialize, Deserialize)]
#[serde(untagged)]
pub enum GenreFields
{
    Name(String),
    Members(Vec<i32>)
}


#[derive(Serialize, Deserialize)]
pub struct SeasonStatistics
{
    #[serde(rename = "TopAnimeScore")]
    pub top_anime_score: HashMap<String, BTreeMap<String, Vec<AnimeEntry>>>,
    #[serde(rename = "TopAnimePopularity")]
    pub top_anime_popularity: HashMap<String, BTreeMap<String, Vec<AnimeEntry>>>,
    #[serde(rename = "TopGenres")]
    pub top_genres: Vec<[GenreFields; 2]>,
    #[serde(rename = "TopCharacters")]
    pub top_characters: Vec<[CharacterOrSeiyuuFields; 2]>,
    #[serde(rename = "TopSeiyuu")]
    pub top_seiyuu: Vec<[CharacterOrSeiyuuFields; 2]>,
}


fn get_string_per_rank(rank: &i32) -> String
{
    match rank {
        1 => "🥇".to_string(),
        2 => "🥈".to_string(),
        3 => "🥉".to_string(),
        _ => format!("{}", rank),
    }
}


impl SeasonStatistics
{
    pub fn load(season: &str, year: i32, full: bool) -> Option<SeasonStatistics>
    {
        let file_path = if full {
            utility::get_data_path()
                .join(format!("{}-{}-full.json", year, season.to_lowercase()))
        } else {
            utility::get_data_path()
                .join(format!("{}-{}.json", year, season.to_lowercase()))
        };

        if let Ok(file) = File::open(file_path) {
            let reader = BufReader::new(file);
            if let Ok(result) = serde_json::from_reader(reader) {
                return Some(result);
            }
        }
        None
    }

    fn get_timestamps_for(map: &HashMap<String, BTreeMap<String, Vec<AnimeEntry>>>, anime_type: &str, is_score: bool) -> Result<Value, Error>
    {
        match map.get(anime_type) {
            None => Ok(Value::String("[]".to_string())),
            Some(records) => {
                let length = records.iter().len();
                if length > 0 {
                    let mut labels = String::with_capacity(length * 13 + 1);
                    labels.push('[');

                    for (timestamp, series) in records.iter() {
                        if series.iter().any(|anime|
                            if is_score {
                                anime.score.is_some()
                            } else {
                                anime.members.is_some()
                            }
                        ) {
                            labels.push_str(format!("\"{}\",", timestamp).as_str());
                        }
                    }

                    if labels.ends_with(',') {
                        labels.pop();
                    }
                    labels.push(']');

                    Ok(Value::String(labels))
                } else {
                    Ok(Value::String("[]".to_string()))
                }
            }
        }
    }

    pub fn get_timestamps_for_top_score_of(&self, anime_type: &str, is_score: bool) -> Result<Value, Error>
    {
        SeasonStatistics::get_timestamps_for(&self.top_anime_score, anime_type, is_score)
    }

    pub fn get_timestamps_for_top_popularity_of(&self, anime_type: &str, is_score: bool) -> Result<Value, Error>
    {
        SeasonStatistics::get_timestamps_for(&self.top_anime_popularity, anime_type, is_score)
    }

    fn get_score_list_for(map: &HashMap<String, BTreeMap<String, Vec<AnimeEntry>>>, anime_type: &str, is_score: bool) -> Result<Value, Error>
    {
        match map.get(anime_type) {
            None => Ok(Value::Array(Vec::new())),
            Some(records) => {
                let length = records.iter().len();
                if length > 0 {
                    let mut list: Vec<Value> = Vec::with_capacity(length);
                    let mut title_per_id: HashMap<i32, String> = HashMap::new();
                    let mut data_per_series: HashMap<i32, Vec<String>> = HashMap::new();
                    let mut padding = 0;

                    for (_timestamp, series) in records {
                        if series.iter().any(|anime|
                            if is_score {
                                anime.score.is_some()
                            } else {
                                anime.members.is_some()
                            })
                        {
                            for anime in series {
                                title_per_id.entry(anime.id)
                                            .or_insert(anime.title.clone());

                                let entry = data_per_series
                                    .entry(anime.id)
                                    .or_insert(vec!["null".to_string(); padding]);
                                if is_score {
                                    entry.push(format!("{}", anime.score.unwrap_or_default()));
                                } else {
                                    entry.push(format!("{}", anime.members.unwrap_or_default()));
                                }
                            }
                            padding += 1;
                        }
                    }

                    for (id, scores) in data_per_series.iter() {
                        let mut data: BTreeMap<String, Value> = BTreeMap::new();

                        data.insert("label".to_string(),
                                    Value::String(title_per_id
                                        .entry(id.clone())
                                        .or_insert("UNKNOWN".to_string())
                                        .to_string())
                        );

                        data.insert("data".to_string(),
                                    Value::String(scores.join(","))
                        );

                        list.push(serde_json::to_value(data)
                            .expect("Cannot serialize 'data'"));
                    }

                    Ok(Value::Array(list))
                } else {
                    Ok(Value::Array(Vec::new()))
                }
            }
        }
    }

    pub fn get_score_list_for_top_score_of(&self, anime_type: &str) -> Result<Value, Error>
    {
        SeasonStatistics::get_score_list_for(&self.top_anime_score, anime_type, true)
    }

    pub fn get_member_list_for_top_popularity_of(&self, anime_type: &str) -> Result<Value, Error>
    {
        SeasonStatistics::get_score_list_for(&self.top_anime_popularity, anime_type, false)
    }

    pub fn get_hottest_characters_or_seiyuu(&self, is_character: bool, full: bool) -> Result<Value, Error>
    {
        let mut list: Vec<Value> = if full {
            Vec::with_capacity(50)
        } else {
            Vec::with_capacity(15)
        };

        let entities = if is_character {
            &self.top_characters
        } else {
            &self.top_seiyuu
        };

        let mut rank = 1;
        for fields in entities {
            let current_rank = rank;
            let delta = match &fields[0] {
                CharacterOrSeiyuuFields::Delta(value) => value,
                _ => panic!("The first field must be 'Delta'")
            };

            if delta <= &0 {
                continue;
            }

            let people = match &fields[1] {
                CharacterOrSeiyuuFields::Entries(value) => value,
                _ => panic!("The second field must be 'Entries'")
            };

            for person in people {
                if list.len() == list.capacity() {
                    break;
                }

                let mut person_data = BTreeMap::new();

                person_data.insert(
                    "rank".to_string(),
                    Value::String(get_string_per_rank(&current_rank))
                );

                person_data.insert(
                    "name".to_string(),
                    Value::String(person.name.clone())
                );

                person_data.insert(
                    "delta".to_string(),
                    Value::Number(delta.clone().into())
                );

                if is_character {
                    person_data.insert(
                        "url".to_string(),
                        Value::String(format!("https://myanimelist.net/character/{}", person.id))
                    );
                } else {
                    person_data.insert(
                        "url".to_string(),
                        Value::String(format!("https://myanimelist.net/people/{}", person.id))
                    );
                }

                person_data.insert(
                    "path".to_string(),
                    Value::String(utility::get_filename_for_character_or_person_picture(person.id, is_character))
                );

                list.push(serde_json::to_value(person_data)
                    .expect("Cannot serialize 'person_data'"));
                rank += 1;
            }
        }

        Ok(Value::Array(list))
    }

    pub fn get_trending_genres(&self) -> Result<Value, Error>
    {
        let mut list: Vec<Value> = Vec::new();

        let mut rank = 1;
        for genre_entry in &self.top_genres {
            let name = match &genre_entry[0] {
                GenreFields::Name(value) => value,
                _ => panic!("The first field must be 'Name'")
            };

            let data = match &genre_entry[1] {
                GenreFields::Members(value) => value,
                _ => panic!("The first field must be 'Members'")
            };

            let mut genre: BTreeMap<String, Value> = BTreeMap::new();

            genre.insert("name".to_string(),
                         Value::String(name.clone()
                                           .replace("_", " ")
                                           .replace("SCI FI", "Sci-Fi")
                                           .replace("SUPER POWER", "Super-power")));

            genre.insert("members".to_string(),
                         Value::String(data[0].clone().to_formatted_string(&Locale::en)));

            genre.insert("series".to_string(),
                         Value::Number(data[1].clone().into()));

            genre.insert("rank".to_string(),
                         Value::String(get_string_per_rank(&rank)));

            list.push(serde_json::to_value(genre)
                .expect("Cannot serialize 'genre'"));
            rank += 1;
        }

        Ok(Value::Array(list))
    }
}


#[derive(Serialize, Deserialize)]
pub struct SeasonalContext
{
    pub start_end: SeasonStartEnd,
    pub statistics: SeasonStatistics,
    pub season: String,
    pub year: i32,
    pub banner: String
}

pub fn get_season_title(parameters: HashMap<String, Value>) -> Result<Value, Error>
{
    let season = parameters.get("season")
                           .expect("Value 'season' not found")
                           .as_str().expect("Invalid format for 'season'")
                           .to_string();

    let year = parameters.get("year")
                         .expect("Value 'year' not found")
                         .as_i64().expect("Invalid format for 'year'");

    let insert_emoji = parameters.get("emoji")
                                 .expect("Value 'emoji' not found")
                                 .as_bool().expect("Invalid format for 'emoji'");

    let emoji = if insert_emoji {
        match season.as_str() {
            "winter" => " ☃",
            "spring" => " 🌸",
            "summer" => " 🌴",
            "fall" => " 🍁",
            "later" => " 🗓",
            _ => " ❓"
        }
    } else {
        ""
    };

    Ok(Value::String(format!("{} {}{}", season, year, emoji)))
}


pub fn get_timestamp_labels(parameters: HashMap<String, Value>) -> Result<Value, Error>
{
    let statistics: SeasonStatistics = serde_json::from_value(parameters.get("stats")
                                                                        .expect("Value 'stats' not found")
                                                                        .to_owned())
        .expect("Cannot deserialize 'stats' into SeasonStatistics");

    let is_score = parameters.get("score")
                             .expect("Value 'score' not found")
                             .as_bool().expect("Invalid format for 'score'");

    let anime_type = parameters.get("type")
                               .expect("Value 'type' not found")
                               .as_str().expect("Invalid format for 'type'")
                               .to_string();

    if is_score {
        statistics.get_timestamps_for_top_score_of(anime_type.as_str(), true)
    } else {
        statistics.get_timestamps_for_top_popularity_of(anime_type.as_str(), false)
    }
}


pub fn get_formatted_anime_data(parameters: HashMap<String, Value>) -> Result<Value, Error>
{
    let statistics: SeasonStatistics = serde_json::from_value(parameters.get("stats")
                                                                        .expect("Value 'stats' not found")
                                                                        .to_owned())
        .expect("Cannot deserialize 'stats' into SeasonStatistics");

    let is_score = parameters.get("score")
                             .expect("Value 'score' not found")
                             .as_bool().expect("Invalid format for 'score'");

    let anime_type = parameters.get("type")
                               .expect("Value 'type' not found")
                               .as_str().expect("Invalid format for 'type'")
                               .to_string();

    if is_score {
        statistics.get_score_list_for_top_score_of(anime_type.as_str())
    } else {
        statistics.get_member_list_for_top_popularity_of(anime_type.as_str())
    }
}


pub fn get_formatted_hottest_characters_or_seiyuu(parameters: HashMap<String, Value>) -> Result<Value, Error>
{
    let statistics: SeasonStatistics = serde_json::from_value(parameters.get("stats")
                                                                        .expect("Value 'stats' not found")
                                                                        .to_owned())
        .expect("Cannot deserialize 'stats' into SeasonStatistics");

    let is_character = parameters.get("character")
                                 .expect("Value 'character' not found")
                                 .as_bool().expect("Invalid format for 'character'");

    let full = parameters.get("full")
                         .expect("Value 'full' not found")
                         .as_bool().expect("Invalid format for 'full'");

    statistics.get_hottest_characters_or_seiyuu(is_character, full)
}


pub fn get_formatted_trending_genres(parameters: HashMap<String, Value>) -> Result<Value, Error>
{
    let statistics: SeasonStatistics = serde_json::from_value(parameters.get("stats")
                                                                        .expect("Value 'stats' not found")
                                                                        .to_owned())
        .expect("Cannot deserialize 'stats' into SeasonStatistics");

    statistics.get_trending_genres()
}


#[derive(Serialize, Deserialize)]
pub struct ArchiveContext
{
    seasons_by_year: Vec<(i32, Vec<String>)>
}


pub fn get_archive_context(data: &SeasonsData) -> ArchiveContext
{
    pub fn previous_season(season: &str) -> Option<String>
    {
        match season {
            "fall" => Some("summer".to_string()),
            "summer" => Some("spring".to_string()),
            "spring" => Some("winter".to_string()),
            _ => None
        }
    }

    let mut list: Vec<(i32, Vec<String>)> = Vec::new();
    let mut year = data.last_year;
    let mut season = Some(data.last_season.clone());

    while year >= data.first_year {
        let mut season_vector = Vec::with_capacity(4);
        while season.is_some() {
            let season_value = season.unwrap();
            season = previous_season(&season_value);

            let should_break = year == data.first_year && season_value == data.first_season;

            season_vector.push(season_value);

            if should_break {
                break
            };
        }

        list.push((year, season_vector));
        year -= 1;
        season = Some("fall".to_string());
    };

    ArchiveContext { seasons_by_year: list }
}
