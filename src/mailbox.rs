use chrono::{Local, NaiveDateTime, Duration, NaiveDate};
use scraper::{Html, Selector, ElementRef};
use serde::Serialize;
use reqwest::Method;

use crate::UsageCache;
use crate::api::{UserData, can_send_requests};
use crate::logging::{Logger, LogMessage};

use std::mem::drop;


pub fn run_mailbox_query(query_option: Option<String>, start_from_page: Option<usize>, user: &mut UserData, usage_cache: UsageCache, logger: Logger) -> Result<QueryResult, String>
{
    if query_option.clone().unwrap_or_default().len() > 200 {
        return Err("Your query cannot exceed 200 characters".to_string());
    }

    match aggregate_query_results(query_option, start_from_page, user, usage_cache, logger.clone())
    {
        Ok(result) => Ok(result.as_query_result()),
        Err(error) => {
            logger.send(LogMessage::Error(format!("Failed to aggregate Mailbox query results: {}", error))).unwrap();
            Err(error)
        }
    }
}


pub fn aggregate_query_results(query_option: Option<String>, start_from_page: Option<usize>, user: &mut UserData, usage_cache: UsageCache, logger: Logger) -> Result<MailboxPage, String>
{
    let mut queried_pages: usize = 0;
    let mut messages_found: usize = 0;
    let query = query_option.unwrap_or_default();
    let mut result = MailboxPage::empty();

    while queried_pages < 8 && messages_found < 20
    {
        let page_option = get_mailbox_page(start_from_page.unwrap_or_default() + queried_pages, user, &usage_cache, logger.clone());
        if page_option.is_err() {
            return page_option;
        }

        let page = page_option
            .unwrap()
            .filter_irrelevant_messages(&query);

        queried_pages += 1;
        messages_found += page.messages.len();
        result.merge_with(page);
    }

    Ok(result)
}


pub fn get_mailbox_page(page: usize, user: &UserData, usage_cache: &UsageCache, logger: Logger) -> Result<MailboxPage, String>
{
    let mut page_cache = user.mailbox_page_cache.lock().expect("Poisoned 'mailbox_page_cache' lock in 'get_mailbox_page'");
    if let Some(cached_page) = page_cache.get(&page)
    {
        Ok(cached_page.clone())
    } else {
        drop(page_cache);

        if let Err(error) = can_send_requests(user.user.id, usage_cache, 1, logger) {
            return Err(error);
        }

        let url = format!("https://myanimelist.net/mymessages.php?go=&show={}", page * 20);

        if let Ok(client) = user.http_client.lock()
        {
            if let Ok(response) = client.request(Method::GET, &url)
                .bearer_auth(&user.token.access_token)
                .send()
            {
                if let Ok(html_text) = response.text()
                {
                    drop(client);
                    let html = Html::parse_document(&html_text);

                    let mailbox_page = MailboxPage {
                        messages: get_message_headers(&html, MailboxType::Received),
                        pages: get_pages(&html).unwrap_or(1),
                        current_page: page,
                        kind: MailboxType::Received,
                    };

                    page_cache = user.mailbox_page_cache.lock().expect("Poisoned 'mailbox_page_cache' lock in 'get_mailbox_page'");
                    page_cache.insert(page, mailbox_page.clone());
                    Ok(mailbox_page)
                } else {
                    Err("Cannot read the body of the HTTP response".to_string())
                }
            } else {
                Err("The HTTP request failed. Is MyAnimeList.net available?".to_string())
            }
        } else {
            Err("Poisoned 'http_client' lock in 'get_mailbox_page'".to_string())
        }
    }
}


fn get_pages(html: &Html) -> Option<usize>
{
    // Find the "Pages (N)..." caption
    let selector = Selector::parse(".fl-r").unwrap();
    let page_html = html.select(&selector).next()?;
    let page_text = page_html.text().collect::<String>();

    let last_digit_index = page_text.trim_start().find(')')?;
    let pages_number_text: String = page_text
        .chars()
        .skip(7)
        .take(last_digit_index - 7)
        .collect();

    if let Ok(pages) = pages_number_text.parse() { Some(pages) } else { None }
}


fn get_message_headers(html: &Html, mailbox_type: MailboxType) -> Vec<MessageHeader>
{
    // Shared HTML selectors
    let headers_selector = Selector::parse(".message").unwrap();
    let replied_selector = Selector::parse(".mym_replied").unwrap();
    let user_selector = Selector::parse(".mym_user").unwrap();
    let a_tag_selector = Selector::parse("a").unwrap();
    let subject_selector = Selector::parse(".subject-link").unwrap();
    let date_selector = Selector::parse(".mym_date").unwrap();

    // Iterate over all the messages
    let mut messages: Vec<MessageHeader> = Vec::with_capacity(20);
    for message_html in html.select(&headers_selector)
    {
        let sender = get_sender(&message_html, &user_selector, &a_tag_selector);
        if let Some((id, subject, opening)) = get_subject_parts(&message_html, &subject_selector) {
            if let Some(time) = get_time(&message_html, &date_selector) {
                if let Some(is_unread) = is_unread(&message_html) {
                    messages.push(MessageHeader {
                        id,
                        sender,
                        subject,
                        opening,
                        time,
                        is_unread,
                        replied: has_replied(&message_html, &replied_selector),
                        mailbox_type,
                    });
                }
            }
        }
    }

    return messages;
}


fn get_sender(message: &ElementRef, user_selector: &Selector, a_tag_selector: &Selector) -> Option<String>
{
    let user_html = message.select(&user_selector).next()?;
    if let Some(username_html) = user_html.select(&a_tag_selector).next() {
        Some(username_html.inner_html().trim().to_string())
    } else { None }
}


fn get_subject_parts(message: &ElementRef, subject_selector: &Selector) -> Option<(u64, String, String)>
{
    let subject_html = message.select(&subject_selector).next()?;

    // Message ID
    let url = subject_html.value().attr("href")?.trim_end_matches("&f=1").to_string();
    let id_index = url.find("&id=")?;
    let id = url[id_index + 4..].parse::<u64>().ok()?;

    // Subject and opening
    let subject_and_header = subject_html.inner_html()
        .trim()
        .replace('\n', " ")
        .replace('\t', " ");
    let mut subject_parts = subject_and_header.split(" - <span class=\"text\">");
    let subject = subject_parts.next()?.to_string();
    let opening = subject_parts.next()?.trim_end_matches("</span>").to_string();

    Some((id, subject, opening))
}


fn get_time(message: &ElementRef, date_selector: &Selector) -> Option<NaiveDateTime>
{
    get_time_from_str(message.select(&date_selector).next()?.inner_html().trim())
}


fn get_time_from_str(time_text_str: &str) -> Option<NaiveDateTime>
{
    let today = Local::today().naive_local();
    let mut time_text = time_text_str.trim().to_string();

    time_text = time_text.replace("Today", &today.format("%b %e").to_string());
    time_text = time_text.replace("Yesterday", &(today - Duration::days(1)).format("%b %e").to_string());

    // Inserting missing year
    if time_text.len() < 19 &&
        !time_text.contains("second") &&
        !time_text.contains("minute") &&
        !time_text.contains("hour") &&
        !time_text.contains("day")
    {
        let comma_index = time_text.find(',')?;
        time_text.insert_str(comma_index + 2, &today.format("%Y").to_string());
    }

    if let Ok(time) = NaiveDateTime::parse_from_str(&time_text, "%b %e, %Y %l:%M %p") {
        return Some(time);
    }

    get_indirect_time(&time_text)
}


fn get_indirect_time(time_text: &str) -> Option<NaiveDateTime>
{
    let now = Local::now().naive_local();

    let mut space_index = time_text.find(" hour");
    if let Some(space_i) = space_index {
        let hours = time_text[..space_i].parse().ok()?;
        return Some(now - Duration::hours(hours));
    }

    space_index = time_text.find(" minute");
    if let Some(space_i) = space_index {
        let minutes = time_text[..space_i].parse().ok()?;
        return Some(now - Duration::minutes(minutes));
    }

    space_index = time_text.find(" day");
    if let Some(space_i) = space_index {
        let days = time_text[..space_i].parse().ok()?;
        return Some(now - Duration::days(days));
    }

    space_index = time_text.find(" second");
    if let Some(space_i) = space_index {
        let seconds = time_text[..space_i].parse().ok()?;
        return Some(now - Duration::seconds(seconds));
    }

    None
}


fn is_unread(message: &ElementRef) -> Option<bool>
{
    Some(message.value().attr("class")?.contains("unread"))
}


fn has_replied(message: &ElementRef, replied_selector: &Selector) -> bool
{
    let replied = message
        .select(&replied_selector)
        .next()
        .and_then(|div|
            Some(!div
                .inner_html()
                .trim()
                .is_empty()));

    match replied {
        Some(result) => result,
        None => false
    }
}


#[derive(Clone)]
pub struct MessageHeader
{
    pub id: u64,
    pub sender: Option<String>,
    pub subject: String,
    pub opening: String,
    pub time: NaiveDateTime,
    pub is_unread: bool,
    pub replied: bool,
    pub mailbox_type: MailboxType,
}


#[derive(Copy, Clone, Eq, PartialEq)]
pub enum MailboxType
{
    Received,
    Sent,
}


#[derive(Clone)]
pub struct MailboxPage
{
    pub messages: Vec<MessageHeader>,
    pub current_page: usize,
    pub pages: usize,
    pub kind: MailboxType,
}


#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct QueryResult
{
    is_last_page: bool,
    next_page: isize,
    messages: Vec<FormattedHeader>,
}


#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct FormattedHeader
{
    id: u64,
    is_unread: bool,
    replied: bool,
    sender: Option<String>,
    subject: String,
    opening: String,
    time: String,
}


impl MailboxPage
{
    fn empty() -> MailboxPage
    {
        MailboxPage {
            messages: Vec::with_capacity(40),
            current_page: 0,
            pages: 0,
            kind: MailboxType::Received,
        }
    }


    fn filter_irrelevant_messages(&self, query: &str) -> MailboxPage
    {
        let mut filtered_page = self.clone();
        let terms: Vec<String> = query
            .split_whitespace()
            .map(|term| term.trim().to_lowercase())
            .filter(|term| !term.is_empty())
            .collect();

        if terms.len() <= 0 {
            return filtered_page;
        }

        filtered_page.messages = filtered_page.messages.iter().filter(
            |message|
                terms.iter()
                    .filter(|term| match_term(term, message))
                    .count() >= terms.len()
        )
            .map(|header| header.clone())
            .collect();

        filtered_page
    }


    fn merge_with(&mut self, other: MailboxPage)
    {
        self.current_page = other.current_page;
        self.pages = other.pages;
        self.kind = other.kind;
        self.messages.extend(other.messages);
    }


    fn as_query_result(&self) -> QueryResult
    {
        QueryResult {
            is_last_page: self.current_page >= self.pages,
            next_page: (self.current_page + 1) as isize,
            messages: self.messages
                .iter()
                .map(|header| FormattedHeader {
                    id: header.id,
                    is_unread: header.is_unread,
                    replied: header.replied,
                    sender: header.sender.clone(),
                    subject: header.subject.clone(),
                    opening: header.opening.clone(),
                    time: header.time.format("%b %-e, %Y - %-H:%M").to_string(),
                })
                .collect(),
        }
    }
}


fn match_term(term: &String, message: &MessageHeader) -> bool
{
    let term_lowercase = term.to_lowercase();

    match_sender(&term_lowercase, message).unwrap_or_default() ||
        match_date(&term_lowercase, message).unwrap_or_default() ||
        match_before(&term_lowercase, message).unwrap_or_default() ||
        match_after(&term_lowercase, message).unwrap_or_default() ||
        match_unread(&term_lowercase, message).unwrap_or_default() ||
        match_read(&term_lowercase, message).unwrap_or_default() ||
        match_unanswered(&term_lowercase, message).unwrap_or_default() ||
        match_replied(&term_lowercase, message).unwrap_or_default() ||
        match_id(&term_lowercase, message).unwrap_or_default() ||

        message.id.to_string().contains(&term_lowercase) ||
        message.sender.clone().unwrap_or_default().to_lowercase().contains(&term_lowercase) ||
        message.subject.to_lowercase().contains(&term_lowercase) ||
        message.opening.to_lowercase().contains(&term_lowercase)
}


fn match_sender(term: &str, message: &MessageHeader) -> Option<bool>
{
    // E.g. "@sender:ZeroCrystal"
    if term.starts_with("@sender:") && term.len() > 9
    {
        Some(term
            .chars()
            .skip(8)
            .collect::<String>()
            ==
            message.sender
                .clone()
                .unwrap_or_default()
                .to_lowercase())
    } else {
        None
    }
}


fn match_date(term: &str, message: &MessageHeader) -> Option<bool>
{
    // E.g. "@date:2021-08-19"
    if term.starts_with("@date:") && term.len() == 6 + 10
    {
        let raw_date = term
            .chars()
            .skip(6)
            .collect::<String>();

        if let Ok(date) = NaiveDate::parse_from_str(&raw_date, "%Y-%m-%d") {
            Some(message.time.date() == date)
        } else {
            None
        }
    } else {
        None
    }
}


fn match_before(term: &str, message: &MessageHeader) -> Option<bool>
{
    // E.g. "@before:2021-08-19"
    if term.starts_with("@before:") && term.len() == 8 + 10
    {
        let raw_date = term
            .chars()
            .skip(8)
            .collect::<String>();

        if let Ok(date) = NaiveDate::parse_from_str(&raw_date, "%Y-%m-%d") {
            Some(message.time.date() <= date)
        } else {
            None
        }
    } else {
        None
    }
}


fn match_after(term: &str, message: &MessageHeader) -> Option<bool>
{
    // E.g. "@after:2021-08-19"
    if term.starts_with("@after:") && term.len() == 7 + 10
    {
        let raw_date = term
            .chars()
            .skip(7)
            .collect::<String>();

        if let Ok(date) = NaiveDate::parse_from_str(&raw_date, "%Y-%m-%d") {
            Some(message.time.date() >= date)
        } else {
            None
        }
    } else {
        None
    }
}


fn match_unread(term: &str, message: &MessageHeader) -> Option<bool>
{
    if term == "@unread" {
        Some(message.is_unread)
    } else {
        None
    }
}


fn match_read(term: &str, message: &MessageHeader) -> Option<bool>
{
    if term == "@read" {
        Some(!message.is_unread)
    } else {
        None
    }
}


fn match_unanswered(term: &str, message: &MessageHeader) -> Option<bool>
{
    if term == "@unanswered" {
        Some(!message.replied)
    } else {
        None
    }
}


fn match_replied(term: &str, message: &MessageHeader) -> Option<bool>
{
    if term == "@replied" {
        Some(message.replied)
    } else {
        None
    }
}


fn match_id(term: &str, message: &MessageHeader) -> Option<bool>
{
    // E.g. "@id:123"
    if term.starts_with("@id:") && term.len() > 4
    {
        Some(term
            .chars()
            .skip(4)
            .collect::<String>()
            ==
            message.id.to_string())
    } else {
        None
    }
}


pub fn get_message(id: u64, user: &mut UserData, usage_cache: UsageCache, logger: Logger) -> Result<Message, String>
{
    let mut message_cache = user.mailbox_message_cache.lock().expect("Poisoned 'mailbox_message_cache' lock in 'get_message'");
    if let Some(cached_message) = message_cache.get(&id)
    {
        Ok(cached_message.clone())
    } else {
        drop(message_cache);

        if let Err(error) = can_send_requests(user.user.id, &usage_cache, 1, logger.clone()) {
            return Err(error);
        }
        drop(usage_cache);

        let url = format!("https://myanimelist.net/mymessages.php?go=read&id={}", id);

        if let Ok(client) = user.http_client.lock()
        {
            if let Ok(response) = client.request(Method::GET, &url)
                .bearer_auth(&user.token.access_token)
                .send()
            {
                if let Ok(html_text) = response.text()
                {
                    drop(client);
                    let html = Html::parse_document(&html_text);

                    let sender = get_message_sender(&html);
                    if let Some(date_time) = get_message_date_time(&html) {
                        if let Some(subject) = get_message_subject(&html) {
                            if let Some(body) = get_message_body(&html) {
                                let message = Message {
                                    id,
                                    sender,
                                    subject,
                                    time: date_time.format("%b %-e, %Y - %-H:%M").to_string(),
                                    body,
                                };

                                message_cache = user.mailbox_message_cache.lock().expect("Poisoned 'mailbox_message_cache' lock in 'get_message'");
                                message_cache.insert(id, message.clone());
                                Ok(message)
                            } else {
                                logger.send(LogMessage::Error(format!("Failed to parse the body of a message (User: {} - {})", user.user.name, user.user.id))).unwrap();
                                Err("Cannot parse the message's body".to_string())
                            }
                        } else {
                            logger.send(LogMessage::Error(format!("Failed to parse the subject of a message (User: {} - {})", user.user.name, user.user.id))).unwrap();
                            Err("Cannot parse the message's subject".to_string())
                        }
                    } else {
                        logger.send(LogMessage::Error(format!("Failed to parse the date of a message (User: {} - {})", user.user.name, user.user.id))).unwrap();
                        Err("Cannot parse the message's date".to_string())
                    }
                } else {
                    logger.send(LogMessage::Error(format!("Failed to parse the body of a HTTP request of a Mailbox's message (User: {} - {})", user.user.name, user.user.id))).unwrap();
                    Err("Cannot read the body of the HTTP response".to_string())
                }
            } else {
                logger.send(LogMessage::Error(format!("Failed to send the HTTP request to access a Mailbox's message. Is MAL down? (User: {} - {})", user.user.name, user.user.id))).unwrap();
                Err("The HTTP request failed. Is MyAnimeList.net available?".to_string())
            }
        } else {
            Err("Poisoned 'http_client' lock in 'get_message'".to_string())
        }
    }
}


fn get_message_sender(html: &Html) -> Option<String>
{
    let dialog_selector = Selector::parse(".dialog-text").unwrap();
    let h2_selector = Selector::parse("h2").unwrap();

    let line = html
        .select(&dialog_selector)
        .next()?
        .select(&h2_selector)
        .next()?
        .inner_html();

    let final_part = line
        .trim()
        .trim_start_matches("Message Sent from ");

    if final_part.contains("Removed user") {
        return None;
    }

    let start_index = final_part.find('>')?;
    let end_index = final_part.find("</a>")?;

    Some(final_part.chars()
        .skip(start_index + 1)
        .take(end_index - start_index - 1)
        .collect())
}


fn get_message_date_time(html: &Html) -> Option<NaiveDateTime>
{
    let dialog_selector = Selector::parse(".dialog-text").unwrap();
    let div_selector = Selector::parse("div").unwrap();

    let time_str = html
        .select(&dialog_selector)
        .next()?
        .select(&div_selector)
        .next()?
        .inner_html();

    get_time_from_str(time_str.as_str())
}


fn get_message_subject(html: &Html) -> Option<String>
{
    let dialog_selector = Selector::parse(".dialog-text").unwrap();
    let div_selector = Selector::parse("div").unwrap();

    Some(html
        .select(&dialog_selector)
        .next()?
        .select(&div_selector)
        .skip(1)
        .next()?
        .inner_html()
        .trim()
        .to_string())
}


fn get_message_body(html: &Html) -> Option<String>
{
    let dialog_selector = Selector::parse(".dialog-text").unwrap();

    let raw_body = html
        .select(&dialog_selector)
        .next()?
        .inner_html();

    let mut body = raw_body.as_str();
    let mut option_index = body.find("fn-grey6\">");
    if option_index.is_none() {
        // The message contains no subject
        option_index = body.find("mb4\">");
    }

    let mut index = option_index?;
    body = &body[index..];

    index = body.find("</div>")?;
    body = body[index + 6..].trim();

    index = body.find("<div class=\"border_top")?;
    body = body[..index].trim();

    Some(body.to_string())
}


#[derive(Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Message
{
    id: u64,
    sender: Option<String>,
    subject: String,
    time: String,
    body: String,
}
